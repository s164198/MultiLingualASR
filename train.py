import os
from argparse import ArgumentParser

import pytorch_lightning as pl
import torch.backends

from model import Transducer


def main(hparams, cluster=None, results_dict=None):
    # noinspection PyUnreachableCode
    if False:
        # I had some issues when using a batch size of 1, and hidden_size 1024. They may be fixed now.
        import torch.backends
        import torch.backends.cudnn
        print(torch.backends.cudnn.version())
        torch.backends.cudnn.enabled = False  # Much slower, but removes the errors.

    model = Transducer(hparams)
    import torch.cuda
    kwargs = {}
    if model.log_level > 0:
        import logging
        logging.getLogger().setLevel(logging.INFO)
    if model.log_level > 1:
        kwargs['row_log_interval'] = 1
        #kwargs['print_nan_grads'] = True
    if hparams.gradients_clip:
        kwargs['gradient_clip_val'] = hparams.gradients_clip
    kwargs['gpus'] = hparams.gpus

    if not hparams.disable_cuda and torch.cuda.is_available():
        trainer = pl.Trainer(**kwargs) # val_percent_check=0.1, val_check_interval=0.1
    else:
        del kwargs['gpus']
        trainer = pl.Trainer(**kwargs)

    trainer.fit(model)

if __name__ == "__main__":
    SEED = 2334
    torch.manual_seed(SEED)
    import numpy as np
    np.random.seed(SEED)

    # TODO: Add argument to resume training?
    root_dir = os.path.dirname(os.path.realpath(__file__))
    parent_parser = ArgumentParser(add_help=False)
    parser = Transducer.add_model_specific_args(parent_parser, root_dir)
    parser.add_argument('--disable-cuda', action='store_true',
                        help='Disable CUDA')
    parser.add_argument('--gradients-clip', default=0, type=float,
                        help='value by which to clip the gradients. 0 == no clip')
    parser.add_argument('--gpus', default=[0], nargs='+', type=int,
                        help='the gpus to use')
    #parser.add_argument('--val-percent-check', default=1.0, type=float,
    #                    help='the amount of validation data to use')
    hparams = parser.parse_args()
    main(hparams)