#!/bin/bash

sudo apt-get install ffmpeg


git clone https://gitlab.gbar.dtu.dk/s164198/MultiLingualASR.git
cd MultiLingualASR
pip install -r requirements.txt

git clone https://github.com/HawkAaron/warp-transducer
mkdir ./warp-transducer/build; cd ./warp-transducer/build; cmake ..; make
cd ../..
sed -i "s@\[os.path.realpath('../include')\]@\[os.path.realpath('../include'), os.path.realpath('$CUDA_HOME/include')\]@g" ./warp-transducer/pytorch_binding/setup.py
cat ./warp-transducer/pytorch_binding/setup.py | grep include_dirs = [
cd ./warp-transducer/pytorch_binding; python setup.py install