import os
import pandas as pd
import tarfile
import zipfile
import urllib

TOP_DIRS = ['TRAIN', 'TEST']
URL = "https://ndownloader.figshare.com/files/10256148"


def convert_timit():
    # Set roots
    source_root = os.path.expanduser(os.path.join("."))
    dest_root = os.path.expanduser(os.path.join("common_voices"))

    # Create directories if needed
    dest_path, dest_clips = create_dest_dir_struct(dest_root)

    # Download and extract archive
    files_root = download_and_extract(source_root)

    entries = []

    for top_dir in TOP_DIRS:
        top_dir_path = os.path.join(files_root, top_dir)
        dr_dirs = os.listdir(top_dir_path)
        for dr_dir in dr_dirs:
            dr_dir_path = os.path.join(top_dir_path, dr_dir)
            inner_dirs = [directory for directory in os.listdir(dr_dir_path) if
                          os.path.isdir(os.path.join(dr_dir_path, directory))]
            for inner_dir in inner_dirs:
                inner_dir_path = os.path.join(dr_dir_path, inner_dir)
                fnames = [file.partition('.')[0] for file in os.listdir(inner_dir_path) if
                          file.lower().endswith(".txt")]

                for fname in fnames:
                    # Gets sentence from text file
                    txt_file_path = os.path.join(inner_dir_path, fname + ".txt")
                    sentence = get_sentence_from_file(txt_file_path)
                    # Construct new_fname
                    new_fname = "{}_{}_{}_{}.wav".format(top_dir, dr_dir, inner_dir, fname)
                    entries.append({"path": new_fname, "sentence": sentence})
                    # Move wav file towards destination
                    wav_file_path = os.path.join(inner_dir_path, fname + ".wav")
                    os.rename(wav_file_path, os.path.join(dest_clips, new_fname))

    df = pd.DataFrame(entries)
    df.to_csv(os.path.join(dest_path, "validated.tsv"), sep='\t', index=False)
    print("Uncompressed files moved in {}.".format(dest_clips))

    with tarfile.open(os.path.join(dest_root, 'raw', 'timit.tar.gz'), "w:gz") as tar:
        tar.add(dest_path, arcname=os.path.basename('timit.tar.gz'))
    print("Tar Archive moved in {}.".format(dest_path))

    print("Done.")


def download_and_extract(root):
    archive_path = os.path.join(root, "TIMIT.zip")

    if not os.path.isfile(archive_path):
        urllib.request.urlretrieve(URL, archive_path)
        print("Archive Downloaded.")

    with zipfile.ZipFile(archive_path, 'r') as zip_ref:
        zip_ref.extractall()
        print("Archive extracted in {}".format(root))

    return os.path.join(root, "data", "lisa", "data", "timit", "raw", "TIMIT")


def get_sentence_from_file(path):
    """
    Gets <str> from one-line file formatted as "<int> <int> <str>".
    """
    with open(path, mode='r') as f:
        line = f.readline()
        line = line.split()

        if len(line) <= 3:
            sentence = ""
        else:
            sentence = " ".join(line[2:])

    return sentence


def create_dest_dir_struct(dest_path):
    # Top-level directory
    dir_path = os.path.join(dest_path, 'uncompressed', 'timit')
    os.makedirs(dir_path, exist_ok=True)
    # Clips directory
    clips_path = os.path.join(dir_path, 'clips')
    os.makedirs(clips_path, exist_ok=True)

    return dir_path, clips_path


if __name__ == '__main__':
    convert_timit()
