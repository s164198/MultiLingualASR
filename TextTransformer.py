import string
import torch
from unidecode import unidecode


class Transformer(torch.nn.Module):
    # TODO: Find better symbol for blank!?
    blank_symbol = '$'  # Warning: Unidecode might strip this character when encoding. (Feature or bug?)

    def __init__(self, alphabet, blank=True):
        super().__init__()  # FIXME: Remove one_hot capability. It is not needed!
        assert self.blank_symbol not in alphabet
        if blank:
            alphabet = self.blank_symbol + alphabet

        # One-Hot or Ordinal encoding. Warning: If added to self, might be uploaded to GPU.
        enc_alphabet = torch.arange(len(alphabet), dtype=torch.int64)

        # Note: The blank is not part of the mapping
        self.mapping = {letter: hot for letter, hot in zip(alphabet, enc_alphabet)}
        self.alphabet = alphabet

    def in_alphabet(self, letter):
        # Same as "letter in self.alphabet", but O(1) instead of O(len(self.alphabet)).
        return letter in self.mapping

    def __len__(self):
        return len(self.mapping)

    def encode(self, word):
        if len(word) == 0: # In case e.g. the whole string was destroyed under the transliterate.
            return torch.empty((0,), dtype=torch.int64)
        return torch.stack([self.mapping[letter] for letter in word])

    def decode(self, indices):
        result = [self.alphabet[idx] for idx in indices] # Look up by index in list.
        return result


class TextTransformer(Transformer):
    def __init__(self, blank=True):
        # Done: Allow blank symbol. It should encode to an all-zero vector.
        alphabet = string.ascii_lowercase + ' '  # Allowed characters, others will be deleted.
        super(TextTransformer, self).__init__(alphabet, blank)

    def transliterate(self, word):
        word = unidecode(word)  # Approximate string with ascii. A type of transliteration.
        word = word.lower()
        word = ' '.join(word.split())  # Now convert all types whitespaces to a simple space. Also strips the string.
        # Now we remove anything not in the alphabet. E.g. digits and punctuation.
        word = filter(self.in_alphabet, word)

        # Note:
        # * It might be better to convert numbers to their words or words to numbers, but this is difficult.
        # Possible fix: Make token that means NUMBER, such that any pronounced number is just made into this?
        # * Also the unidecode may make two words have the same text-representation. E.g. 'lake' and 'a' in danish.
        # Possible fix: A more accurate transliteration library would have to be used.
        return ''.join(word).strip()  # Turn it from filter to string.

    def encode(self, word):
        return super().encode(self.transliterate(word))

    def decode(self, onehots):
        #if len(onehots) == 0:
        #    return ''
        return ''.join(super().decode(onehots))  # Make list of letters into string.

    def __call__(self, text):
        encoded = self.encode(text)
        return encoded


def test_ex1():
    from onehot import OneHot
    dataset = CommonVoiceDataset(root='.', name='rw', download=True)

    oneh = OneHot(dataset.languages)
    word = [oneh.alphabet[0]]  # In a batch this would be a list of languages.
    encoded = oneh.encode(word)
    decoded = oneh.decode(encoded)
    print('Ex1: Language vector.')
    print('Alphabet:', oneh.alphabet)
    print('word:', word)
    print('encoded word shape:', encoded.shape)
    print('decoded word:', decoded)


def test_ex2():
    from collections import Counter
    from onehot import OneHotText
    dataset = CommonVoiceDataset(root='.', name='rw', download=True)
    oneh = OneHotText()

    # TODO Get the path to the audio file for the first time a character is spoken.
    sample_rates, cis, trans = Counter(), Counter(), Counter()
    for audio, text in dataset:
        cis.update(text.casefold())  # casefold is like lowercase but takes care of caseless letters.
        trans.update(oneh.transliterate(text))
        sample_rates.update(audio['sample_rate'])

    print('Original distribution:', cis)
    print('Transliterated distribution:', trans)
    removed = set(cis.keys()) - set(trans.keys())  # Gets letters in original text, and not in transliteration.
    print('The transliterate-removed symbols:', {r: cis[r] for r in removed})
