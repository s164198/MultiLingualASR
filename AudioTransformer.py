import torch
import librosa


def window_stack(a, stepsize=1, width=8):
    # https://stackoverflow.com/questions/15722324/sliding-window-of-m-by-n-shape-numpy-ndarray
    n = a.shape[0]
    return torch.stack([a[i:1 + n + i - width:stepsize] for i in range(0, width)], 1)


class AudioTransformer(torch.nn.Module):

    def __init__(self, n_mffc=80, hop_time=0.010, window_time=0.025, stack=8, hop=3, trim_db=40):
        super().__init__()
        self.n_mfcc = n_mffc
        self.hop_time = hop_time
        self.window_time = window_time
        self.stack = stack
        self.hop = hop
        self.trim_db = trim_db

    def __call__(self, audio, sr):

        # Trim audio
        audio, _ = librosa.effects.trim(y=audio, top_db=self.trim_db)

        # Compute MFCC args
        hop_length = int(self.hop_time * sr)
        window_length = int(self.window_time * sr)
        n_fft = self.next_power_of_2(window_length)

        # Transform audio
        transformed_audio = librosa.feature.mfcc(y=audio, sr=sr, n_mfcc=self.n_mfcc, n_fft=n_fft,
                                                 win_length=window_length, hop_length=hop_length)
        transformed_audio = torch.tensor(transformed_audio.T)  # Note the transposed
        # Now audio is mel-features spaced with 10 ms between.

        # Stack frames... This can be made more efficient.
        transformed_audio = window_stack(transformed_audio, width=self.stack)  # Stack 8 frames
        transformed_audio = transformed_audio.reshape(-1, self.stack * self.n_mfcc)[::self.hop]  # Downsample

        return transformed_audio

    @staticmethod
    def next_power_of_2(x):
        return 1 if x == 0 else 2 ** (x - 1).bit_length()
