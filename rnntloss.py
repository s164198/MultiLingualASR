import torch
import torch.nn as nn
import torch.nn.functional as F


class RNNTLoss(nn.Module):
    """
    Parameters:
        blank (int, optional): label used to represent the blank symbol. Default: 0.
        reduction (string, optional): Specifies the reduction to apply to the output:
            'none' | 'mean' | 'sum'. 'none': no reduction will be applied,
            'mean': the output losses will be divided by the target lengths and
            then the mean over the batch is taken. Default: 'mean'
        backend (int): Specifies the backend to use:
            0: Use the HawkAaron/warp-transducer backend.
            1: Use the 1ytic/warp-rnnt backend.
            Default: 0

    Created from https://github.com/HawkAaron/warp-transducer/blob/c6d12f9e1562833c2b4e7ad84cb22aa4ba31d18c/pytorch_binding/warprnnt_pytorch/__init__.py
    and https://github.com/1ytic/warp-rnnt/blob/542bfb7f0304c4a5e5f0f0a60ff85085bfe2e736/pytorch_binding/warp_rnnt/__init__.py
    Note the similarity to warprnnt_pytorch and https://github.com/SeanNaren/warp-ctc/blob/pytorch_bindings/pytorch_binding/warpctc_pytorch/__init__.py
    """

    def __init__(self, blank=0, reduction='mean', backend=0):
        super(RNNTLoss, self).__init__()
        self.blank = blank
        self.reduction = reduction  # Note 'mean' may not be implemented completely correctly yet.
        # We have to move imports to here since maybe they're not both installed on the system.
        if backend == 0:
            # warp-transducer
            from warprnnt_pytorch import rnnt_loss
        elif backend == 1:
            # warp-rnnt
            from warp_rnnt import rnnt_loss
        else:
            raise ValueError('Backend must be 0 (warp-transducer) or 1 (warp_rnnt).')

        self._rnnt_loss = rnnt_loss
        self.backend = backend

    def forward(self, acts, labels, act_lens, label_lens):
        """
        acts (torch.Tensor): Tensor of (batch x seqLength x labelLength x outputDim) containing output from network
        labels (torch.IntTensor): 2 dimensional Tensor containing all the targets of the batch with zero padded
        act_lens (torch.IntTensor): Tensor of size (batch) containing size of each output sequence from the network
        label_lens (torch.IntTensor): Tensor of (batch) containing label length of each example.
        """

        if (self.backend == 0 and not acts.is_cuda) or self.backend == 1:
            # Must be manually done for CPU version of warp-transducer,
            # log_softmax is [automatically] computed within GPU version.
            acts = F.log_softmax(acts, -1)

        loss = self._rnnt_loss(acts, labels, act_lens, label_lens, blank=self.blank, reduction=self.reduction)

        # Might need something like costs = costs / label_lens.to(log_probs) if reduction is mean.

        if self.backend == 0 and self.reduction in ['sum', 'mean']:
            loss.squeeze_(0)  # Usually a reduced loss should have shape () and not (1,).

        return loss
