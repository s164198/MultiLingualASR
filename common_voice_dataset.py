import librosa
import torch
import torch.utils.data as data
import os
import tarfile
import urllib
import errno
import pandas as pd
import numpy as np
import h5py
import multiprocessing
import functools
from TextTransformer import TextTransformer
from AudioTransformer import AudioTransformer
from PadTextAudio import PadTextAudio

import warnings

warnings.filterwarnings("ignore")

"""
NOTE:   To handle the language correctly, we will take the convention that 
        each CommonVoiceDataset's has to be initialized with an `idx` param. 
        That number will be returned along the text and audio, when the __getitem__
        method is called. That way, the wrapper ConcatSet can remain simple and 
        return multilingual audio and text, along with the correct language representation.
"""


def process_audio_entry(audio_text, root_path, audio_transformer, text_transformer):
    audio_url, text = audio_text
    # Load corresponding audio
    audio_path = os.path.join(root_path, audio_url)
    audio, sr = librosa.load(audio_path, None)

    # Transform
    if audio_transformer is not None:
        audio = audio_transformer(audio, sr)
    if text_transformer is not None:
        text = text_transformer(text)

    return {"audio": audio, "text": text}


class CommonVoiceDataset(data.Dataset):
    base_url = "https://voice-prod-bundler-ee1969a6ce8178826482b88e843c335139bd3fb4.s3.amazonaws.com/cv-corpus-3/{}.tar.gz"
    allowed_languagues = ('fr', 'it', 'en', 'rw', 'sv-SE', 'tt', 'tr', 'timit')
    tsv_file = 'validated.tsv'
    tsv_file_cols = ["path", "sentence"]
    sep = "\t"
    n_splits = 10

    def __init__(self, root, language, idx=0, audio_transformer=None, text_transformer=None,
                 num_workers=os.cpu_count() - 1, max_length=110):
        self.root = os.path.join(os.path.expanduser(os.path.join(root)), 'common_voices')
        self.path = os.path.join(self.root, "{}_{}.h5".format(language, max_length))
        self.raw_path = os.path.join(self.root, 'raw')
        self.uncompressed_path = os.path.join(self.root, 'uncompressed')

        self.language = language
        self.language_idx = idx

        self.audio_transformer = audio_transformer
        self.text_transformer = text_transformer
        self.num_workers = num_workers
        self.max_length = max_length

        if not self._check_exists():
            self._download()
            self._create_dataset()

    def __getitem__(self, item):
        with h5py.File(self.path, 'r') as f:
            audio = torch.tensor(f["audios"][str(item)][:])
            text = torch.tensor(f["texts"][str(item)][:])

        return audio, text, self.language_idx

    def __len__(self):
        with h5py.File(self.path, 'r') as f:
            length = len(f["audios"])
        return length

    def _check_exists(self):
        return os.path.exists(self.path)

    def _download(self):

        dest_path = os.path.join(self.uncompressed_path, self.language)
        # Create directory structure
        for dir_path in [self.raw_path, self.uncompressed_path, dest_path]:
            try:
                os.makedirs(dir_path)
            except OSError as e:
                if e.errno == errno.EEXIST:
                    continue
                else:
                    raise

        url = self.base_url.format(self.language)
        archive_name = url.rpartition('/')[2]
        archive_path = os.path.join(self.raw_path, archive_name)

        # Download if needed
        if not os.path.isfile(archive_path):
            print("-- Downloading {}. --".format(url))
            urllib.request.urlretrieve(url, archive_path)
        else:
            print("File {} already downloaded.".format(url))

        if not os.listdir(dest_path):
            with tarfile.open(archive_path) as arch_f:
                arch_f.extractall(dest_path)
            print("Archive {} extracted in {}.".format(archive_name, dest_path))
        else:
            print("Folder {} is not empty.".format(dest_path))

    def _create_dataset(self):
        df = pd.read_csv(os.path.join(self.uncompressed_path, self.language, self.tsv_file), sep=self.sep,
                         usecols=self.tsv_file_cols, encoding='utf-8')
        entries = df.values
        preprocess = functools.partial(process_audio_entry,
                                       root_path=os.path.join(self.uncompressed_path, self.language,
                                                              "clips"),
                                       audio_transformer=self.audio_transformer,
                                       text_transformer=self.text_transformer)

        with h5py.File(self.path, 'w', libver='latest') as h5f:

            # Create groups
            h5f.create_group("audios")
            h5f.create_group("texts")

            pool = multiprocessing.Pool(self.num_workers)
            i = 0
            for s, split in enumerate(np.array_split(entries, self.n_splits, axis=0)):

                preprocessed = pool.map(preprocess, split)

                for p in preprocessed:
                    if len(p["audio"]) <= self.max_length:
                        h5f["audios"].create_dataset(str(i), data=p["audio"])
                        h5f["texts"].create_dataset(str(i), data=p["text"])
                        i += 1
                h5f.flush()
                print("[{}/{}] Dataset Preparation".format(s + 1, self.n_splits))

            # Note: Necessary for further readings.
            h5f.swmr_mode = True

        print("Dataset prepared and located in {}".format(self.path))


class ConcatSet(data.Dataset):
    def __init__(self, subsets: list):
        self.subsets = subsets
        self.bins = np.cumsum(list(map(len, self.subsets)))
        # NOTE: store to avoid recomputation

    def __getitem__(self, index):
        dataset_index = (self.bins - 1 < index).sum()  # -1 because we want last index and not size
        dataset_offset = index - self.bins[dataset_index - 1] if dataset_index > 0 else index
        return self.subsets[dataset_index][dataset_offset]

    def __len__(self):
        return sum(map(len, self.subsets))


def split_dataset(dataset, val_prop=.1, test_prop=.1, random_seed=42):
    assert val_prop + test_prop < 1.0

    torch.manual_seed(random_seed)

    full_size = len(dataset)
    val_size = int(val_prop * full_size)
    test_size = int(test_prop * full_size)
    train_size = full_size - val_size - test_size

    return torch.utils.data.random_split(dataset, [train_size, val_size, test_size])


def test_concatset():
    langs = ['it', 'fr']

    tt = TextTransformer()
    at = AudioTransformer()

    print("Create datasets: {}".format(langs))
    ds = [CommonVoiceDataset(".", lang, idx=i, audio_transformer=at, text_transformer=tt, max_length=200) for i, lang in
          enumerate(langs)]

    splits = [split_dataset(d) for d in ds]

    train_sets = [s[0] for s in splits]
    train = ConcatSet(train_sets)

    train_loader = data.DataLoader(train, batch_size=10, collate_fn=PadTextAudio(), num_workers=5, shuffle=True)
    for i, batch in enumerate(train_loader):
        audios, texts, audios_lens, texts_lens, langs = batch

        print(audios.shape)
        print(texts.shape)
        print(audios_lens)
        print(texts_lens)
        print(langs)

        if i == 10:
            break


def test_timit(root='.'):
    path_to_archive = os.path.join(root, "common_voices", "raw", "timit.tar.gz")
    assert os.path.exists(path_to_archive), "convert_timit.py should be run first."

    lang = 'timit'

    tt = TextTransformer()
    at = AudioTransformer()

    ds = CommonVoiceDataset(".", lang, audio_transformer=at, text_transformer=tt, max_length=200)

    train_loader = data.DataLoader(ds, batch_size=10, collate_fn=PadTextAudio(), num_workers=5, shuffle=True)
    for i, batch in enumerate(train_loader):
        audios, texts, audios_lens, texts_lens, langs = batch

        print(audios.shape)
        print(texts.shape)
        print(audios_lens)
        print(texts_lens)
        print(langs)

        if i == 25:
            break


if __name__ == '__main__':
    test_concatset()
