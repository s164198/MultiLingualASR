import torch
from torch import autograd
import torch.nn.functional as F


import librosa
import numpy as np
#import seaborn as sns
#sns.set(context="poster")
import matplotlib.pyplot as plt
from matplotlib import cm
import pytorch_lightning as pl
import librosa.display
from matplotlib.ticker import FixedLocator

from AudioTransformer import AudioTransformer, window_stack
from PadTextAudio import PadTextAudio
from TextTransformer import TextTransformer
from common_voice_dataset import CommonVoiceDataset, split_dataset, process_audio_entry, ConcatSet
import torch
import torch.nn
import torch.nn.functional as F
from argparse import Namespace

class Sequence:
    def __init__(self, seq=None, blank=0):
        if seq is None:
            self.g = []  # predictions of grapheme language model
            self.k = [blank]  # prediction grapheme label
            self.h = None # input hidden vector to grapheme model
            self.logp = 0  # probability of this sequence, in log scale
        else:
            self.g = seq.g[:]  # save for prefixsum, [:] is shallow list copy.
            self.k = seq.k[:]
            self.h = seq.h
            self.logp = seq.logp

import math


def log_aplusb(a, b):
    return max(a, b) + math.log1p(math.exp(-math.fabs(a - b)))


def beam_search(rnnt, xs, language_vector, W=10, prefix=False, length_normalize=True):
    """
    `xs`: acoustic model outputs
    NOTE only support one sequence (batch size = 1)
    """
    use_gpu = xs.is_cuda
    label = xs.new_ones((1,), dtype=torch.int64)
    label = F.one_hot(label[:, None], rnnt.vocab_size)
    label = rnnt.add_language_vector(label, language_vector).to(dtype=xs.dtype)
    label_len = xs.new_ones((1,), dtype=torch.int32)  # Literal 1 length. We advance one token at a time.
    if use_gpu:
        label_len = label_len.cuda()
        label = label.cuda()

    def forward_step(label_pred, hidden):
        """ `label`: int """
        # Modify the one-hot vector.
        label[..., :rnnt.vocab_size].zero_()
        label[..., label_pred] = 1
        pred, hidden = rnnt._decoder(label, label_len, hidden)
        return pred, hidden

    def is_prefix(a, b):
        # a is the prefix of b
        if len(a) >= len(b) or a == b: return False  # Second clause is redundant?
        for i in range(len(a)):
            if a[i] != b[i]: return False
        return True

    #xs = rnnt._encoder(xs, xs_len)[0]
    B = [Sequence()]
    B[0].g.append(forward_step(B[0].k[-1], B[0].h)[0])  # Add initial pred to initial seq.
    for t, x in enumerate(xs):
        x = x[None, None] # add batch and sequence dimensions
        A = B
        B = []
        if prefix:
            A.sort(key=lambda a: len(a.k), reverse=True)  # longest sequence first (maybe the other way?)
            # for y in A:
            #     y.logp = log_aplusb(y.logp, prefixsum(y, A, x))
            for j in range(len(A) - 1):
                for i in range(j + 1, len(A)):  # Possible prefixes, shortest first.
                    if not is_prefix(A[i].k, A[j].k):
                        continue
                    # A[i] -> A[j]
                    # Calculate values P(k|y[:u-1], t) for k in vocab and u=|y_hat|..|y|.

                    # All in A are the estimated probabilities at time t-1.
                    # Now for prefix we allow the probability of skipping directly A[i] -> A[j], i.e. this prefix part
                    #  is to allow for multiple tokens emitted at a single audio input.
                    next_steps = torch.cat(A[j].g[len(A[i].k):], 1)
                    ytu = rnnt._joint(x, next_steps)[0, 0, :]  # 0 batch, 0 audio-seq, text-seq
                    logps = F.log_softmax(ytu, dim=-1).cpu()
                    idxs = torch.as_tensor(A[j].k[len(A[i].k):])
                    logp = A[i].logp + logps.gather(-1, idxs[:, None])[:, 0].sum(0) # Extract P(y[u]|y[:u-1]) values.
                    # logp = A[i].logp + sum(logps[u, yu] for u, yu in enumerate(A[j].k[len(A[i].k):])) # Same as above.
                    #print(j, A[j].k, A[i].k)
                    probability = float( logp ) # P(y_hat)*P(y|y_hat,t) in log space.
                    # Ends up adding to more than 1.
                    # My guess is that this is the probability of moving from A[i] to A[j] in one time-step, but then
                    #  it does not take into account all the blank tokens there must be in between for this to happen.

                    # A BETTER definition might be
                    #  Pr(y|y_hat,t) = RNNTLoss(Joint(Pred(y[|y_hat|:] | y_hat), Enc(0:t)), y[|y_hat|:])
                    # This is if we don't care about the exact alignment we found in y. We probably do.
                    #A[j].logp = log_aplusb(A[j].logp, A[j].base_logp - A[j].logp_hist[len(A[i].k)-1] + A[i].logp)
                    # It also seems an issue is that we modify, and add to this probability for each iteration.

                    A[j].logp = float(np.logaddexp(A[j].logp, probability))
                    #A[j].logp = log_aplusb(A[j].base_logp, probability ) # Same as above.

        most_probable_idx = np.argmax([a.logp for a in A])
        while sum( np.array([a.logp for a in B]) >= A[most_probable_idx].logp ) < W:
            # y* = most probable in A
            y_hat = A.pop(most_probable_idx)
            # calculate P(k|y_hat, t)
            # get last label and hidden state
            pred, hidden = forward_step(y_hat.k[-1], y_hat.h)
            ytu = rnnt._joint(x, pred)[0,0,0] # 0 batch, 0 audio-seq, 0 text-seq
            logp = F.log_softmax(ytu, dim=-1).cpu()  # log probability for each k
            for k in range(len(logp)):  # len == rnnt.vocab_size
                yk = Sequence(y_hat)
                yk.logp += float(logp[k])
                if k == 0:  # 0 == blank
                    B.append(yk)  # next move
                    continue
                # store prediction distribution and last hidden state
                yk.h = hidden
                yk.k.append(k)
                if prefix: yk.g.append(pred)
                A.append(yk)

            most_probable_idx = np.argmax([a.logp for a in A])

        # beam width
        B.sort(key=lambda a: a.logp, reverse=True)
        B = B[:W]

    # return highest probability sequence
    best = B[0]
    if length_normalize:
        best = max(B, key=lambda a: a.logp / len(a.k)) # Length normalized, as in the paper.
    return best.k, -best.logp

if __name__ == "__main__":
    import os, torch

    path = os.path.join('.', 'pretrained', 'french')
    from model import Transducer
    model = Transducer.load_from_checkpoint(os.path.join(path, 'checkpoints', '_ckpt_epoch_2.ckpt'))
    # trainer = pl.Trainer()
    # model.hparams.beam_width = 1
    # trainer.test(model)

    audio_transformer = AudioTransformer()
    text_transformer = TextTransformer()
    collate_transformer = PadTextAudio()

    #text = '"Qu\'est-ce qu\'ils étudient ?"'
    #audio_file = 'common_voice_fr_17403001.mp3'
    #audio_path = os.path.join('.', 'common_voices', 'uncompressed', 'fr', 'clips')

    #text_translit = text_transformer.transliterate(text)
    #out = process_audio_entry(audio_text=(audio_file, text), root_path=audio_path,
    #                          audio_transformer=audio_transformer, text_transformer=text_transformer)
    #text_enc, mfcc = out['text'], out['audio']

    #print('Original text:', text)
    #print('Unicoded text:', text_translit)
    #print('Encoded text:', *map(lambda t: t.item(), text_enc))

    #audio, sr = librosa.load(os.path.join(audio_path, audio_file), None)
    #audio_trimmed, trim_idx = librosa.effects.trim(y=audio, top_db=audio_transformer.trim_db)

    import torch.utils.data as data
    from jiwer import wer
    import editdistance

    ds = [CommonVoiceDataset(".", lang, idx=i, audio_transformer=model.audio_transformer,
                             max_length=model.hparams.max_audio_length, num_workers=0,
                             text_transformer=model.text_transformer) for i, lang in enumerate(model.hparams.languages)]
    splits = [split_dataset(d) for d in ds]
    # Concat train, val and test
    train_set = ConcatSet([s[0] for s in splits])
    val_set = ConcatSet([s[1] for s in splits])
    _ = ConcatSet([s[2] for s in splits])
    val_loader = data.DataLoader(val_set, batch_size=1, # model.hparams.batch_size
                                      collate_fn=collate_transformer, num_workers=0)
    #data.DataLoader(ds, batch_size=10, collate_fn=PadTextAudio(), num_workers=5, shuffle=True)
    model.eval()
    model = model.to('cuda')
    W = 50
    print('W is', W)

    for idx, batch in enumerate(val_loader):
        audio_padded, text_padded, audio_lengths, text_lengths, languages = (t.to('cuda', non_blocking=True) for t in batch)

        language_vectors = F.one_hot(languages, model.num_languages)
        if model.hparams.bn_audio:
            audio_padded = model.bn(audio_padded.reshape(-1, audio_padded.shape[-1])).reshape(*audio_padded.shape)
        encoder_state, _ = model._encoder(model.add_language_vector(audio_padded, language_vectors), audio_lengths)
        with torch.no_grad():
            bk, bp = beam_search(model, encoder_state[0], language_vectors[:1], W = W, prefix=False)
        gt = text_transformer.decode(text_padded[0, :text_lengths[0]])
        pred = text_transformer.decode(bk[1:])
        print(editdistance.eval(gt, pred) / len(gt), gt, ':', pred, bp, wer(gt, pred), )
    #beam_search(model, mfcc[None])
