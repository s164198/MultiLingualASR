import csv


def remove_non_alpha(s):
    """
    Removes all the non-alpha chars from a string.
    Returns the resulting string in lower case.
    """
    delchars = ''.join(c for c in map(chr, range(8364)) if not c.isalpha())
    transtab = str.maketrans('', '', delchars)
    return s.translate(transtab).lower()


def merge_tsv(sources_fnames, destination_fname, alphabet_fname, fieldnames=("path", "sentence"),
              alphabet_source="sentence", alphabet_trim_fn=None, delimiter='\t'):
    """
    Merges TSV files into a unique one.
    Figures out the alphabet and puts it into a specified file.
    Assumes that all the source files have the same header.

    Returns:
    --------
      :alphabet: (str)
    """
    assert len(sources_fnames) >= 1
    assert alphabet_source in fieldnames

    alphabet = set()

    with open(destination_fname, "a")  as dest_f:
        writer = csv.DictWriter(dest_f, fieldnames=fieldnames, delimiter=delimiter)
        writer.writeheader()

        for fpath in sources_fnames:
            with open(fpath, "r") as f:
                csv_reader = csv.DictReader(f, delimiter=delimiter)
                for r in csv_reader:
                    writer.writerow({f: r[f] for f in fieldnames})
                    sentence = r[alphabet_source]

                    if alphabet_trim_fn is not None: sentence = alphabet_trim_fn(sentence)

                    alphabet.update(list(sentence))

    alphabet = ''.join(str(e) for e in alphabet)
    with open(alphabet_fname, "a") as alpha_f:
        alpha_f.write(alphabet)

    return alphabet
