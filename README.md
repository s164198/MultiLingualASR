# MultilingualASR

To reproduce our results run the [ReproduceResults.ipybn](ReproduceResults.ipynb) notebook.

Pretrained models can be downloaded [here](https://dtudk-my.sharepoint.com/:u:/g/personal/s164198_win_dtu_dk/Ecm_3tbWG6VApYvtA9JAr50B3wtX8Q4Yc3K4wlDMJeGf-Q?e=U5l9NV). (Only available for DTU students and faculty)


## Project description
The final report is available [here](Multilingual_ASR.pdf)!

The project description can be found [here](statement.pdf).

The project poster can be found [here](pster.pdf).

## Useful links
### Literature
* Google paper we are referencing [here](https://arxiv.org/abs/1909.05330) and corresponding blog post [there](https://ai.googleblog.com/2019/09/large-scale-multilingual-speech.html);

* RNN-T paper [here](https://arxiv.org/pdf/1211.3711);

* Common Voice [here](https://arxiv.org/abs/1912.06670);

### Misc.
* Poster LaTeX template [here](https://github.com/rafaelbailo/betterposter-latex-template), see [there](https://www.youtube.com/watch?v=1RwJbhkCA58&feature=youtu.be) the motivation behind the *unconventional* layout. 
