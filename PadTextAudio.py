import numpy as np
import torch


class PadTextAudio:

    def __call__(self, batch):
        """
        :param batch: (list of tuple (example, audio))
        :return:(Tensor, n_samples*n_steps*n_features) Padded audio;
                (Tensor, n_samples*n_steps*n_features) Padded text;
                (Tensor) list of audios lengths;
                (Tensor) list of texts lengths;
                (list) list of languages.
        """

        audios, texts, audios_lens, texts_lens, i_langs = [], [], [], [], []

        for audio, text, i_lang in batch:
            audios.append(audio)
            texts.append(text)
            audios_lens.append(len(audio))
            texts_lens.append(len(text))
            i_langs.append(i_lang)

        audios_lens = torch.tensor(audios_lens, dtype=torch.int32)
        texts_lens = torch.tensor(texts_lens, dtype=torch.int32)
        i_langs = torch.tensor(i_langs, dtype=torch.int64)  # torch.nn.functional.one_hot needs int64

        # I think we don't have to pad sequences
        # Padded
        padded_audios = torch.nn.utils.rnn.pad_sequence(audios, batch_first=True)
        padded_texts = torch.nn.utils.rnn.pad_sequence(texts, batch_first=True)

        return padded_audios, padded_texts, audios_lens, texts_lens, i_langs


def test1():
    print('Test1: Inverse permutation')
    # These represent the lengths of the sequences (in audio/text-time).
    original = np.arange(5, dtype=np.int32)
    permuted = np.flip(original)
    permuted[0], permuted[1] = permuted[1], permuted[0]
    print('Original', original)
    print('Permutation', permuted)

    inverse = permuted.argsort()
    print('Permutation inverse', inverse)
    print('It is truly the inverse, result:', permuted[inverse])

    inverse_1 = np.empty_like(permuted)
    for a, b in enumerate(permuted):
        inverse_1[b] = a

    # The implementation we should use to invert a permutation:
    inverse_2 = np.empty_like(permuted)
    inverse_2[permuted] = range(len(permuted))

    print('Also the inverse:', inverse_1, inverse_2)


if __name__ == "__main__":
    pta = PadTextAudio()

    a1, a2, a3 = torch.Tensor([4, 4, 4, 4]).resize_((4, 1)), torch.Tensor([2, 2]).resize_((2, 1)), torch.Tensor(
        [5, 5, 5, 5, 5]).resize_((5, 1))
    t1, t2, t3 = torch.Tensor([1]).resize_((1, 1)), torch.Tensor([3, 3, 3]).resize_((3, 1)), torch.Tensor(
        [6, 6, 6, 6, 6, 6]).resize_((6, 1))

    batch = [{"audio": a1, "target": t1}, {"audio": a2, "target": t2}, {"audio": a3, "target": t3}]

    audio, text = pta(batch)

    print(audio, text)

    # First element of the batch should be a1:t1
    assert np.allclose(audio[0, :4, :], a1)
    assert np.allclose(text[0, :1, :], t1)

    # Second element of the batch should be a2:t2
    assert np.allclose(audio[1, :2, :], a2)
    assert np.allclose(text[1, :3, :], t2)
