import os
from argparse import Namespace

import pytorch_lightning as pl

from model import Transducer
import sys
import numpy as np

def newest(path):
    files = os.listdir(path)
    paths = [os.path.join(path, basename) for basename in files]
    return max(paths, key=os.path.getctime)

import torch

class BeamSearchNode:
    def __init__(self, log_prob=0, token=torch.zeros((1, 1), dtype=torch.int64),
                 hidden=None, parent=None, owner=None, length=0):
        assert parent is None or hidden is not None
        if parent is not None:
            self.log_prob = log_prob + parent.log_prob
            owner = parent.owner
            if token:  # If not blank
                length = parent.length + 1
                self.hidden = hidden
            else:
                length = parent.length
                self.hidden = parent.hidden
        else:
            self.hidden = None  # Only in case we are at root. Should be simplified.
            self.log_prob = 0

        self.token = token
        self.parent = parent
          # FIXME: Hidden should be initialized in any case.
        self.length = length
        self.owner = owner

    def recurse(self):
        if self.parent is None:
            return []
        lst = self.parent.recurse()
        if self.token:  # If not blank
            lst.append(self.token.item())
        return lst

    def score(self):
        # The heapq is ordered with the smallest item as root.
        value = self.log_prob  # Higher probability means this is smaller (it is strictly positive)
        # The loss can be shaped here
        #if self.length > 0:
        #    value /= self.length  # To have a fair comparison we take the geometric average.
        return value

    def __lt__(self, other):
        return self.score() < other.score()



import heapq
import torch.nn.functional as F

import itertools
def split_seq(iterable, size):
    """Yield successive size-sized chunks from the iterable."""
    # https://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks
    it = iter(iterable)
    item = list(itertools.islice(it, size))
    while item:
        yield item
        item = list(itertools.islice(it, size))

def to_cpu(tensor):
    if tensor is None:
        return None
    if isinstance(tensor, list) or isinstance(tensor, tuple):
        return list(map(torch.Tensor.cpu, tensor))



def beam_decode(network: Transducer, audio_padded, audio_lengths: torch.IntTensor, languages,
                width=1, max_length=100):  # FIXME: width and max_length should be determined
    # Remember to turn off autograd when running this or you may get a huge memory leak. (This is intended)
    assert audio_padded.shape[0] == 1  # No batch. (Yet!)
    # FIXME if width is 1, the result should be the same as for a beam search.
    # The audio_padded should be shape [1, T, 640]
    language_vectors = F.one_hot(languages, network.num_languages)

    # The encoding of audio can be done in one step. (No need to track hidden state then)
    encoder_input = network.add_language_vector(audio_padded, language_vectors)
    encoder_state, _ = network._encoder(encoder_input, audio_lengths)

    one = torch.ones((1,), dtype=torch.int32, device=audio_padded.device)  # Sequence length is always 1.

    # The beam search list is one frontier for each element in the batch. len = batch size
    #  The beam search frontier is a list of the *width* most promising beam seach nodes. len <= width
    #   The beam search nodes are tuples of (score, token, hidden, parent). len = 4
    frontier_list = [[BeamSearchNode(owner=i)] for i in range(audio_padded.shape[0])]
    new_frontier_list = [[]] * audio_padded.shape[0]

    # Every batch is processed independently, but we can concatenate their frontiers and put them through the network.
    lengths: np.array = audio_lengths.cpu().numpy()
    for t in range(audio_padded.shape[1]):
        active = t < lengths # Get the batch-elements whose audio sequences are not over.
        #active = active.nonzero()
        # Mutate frontiers. Nested comprehensions are hard to read.
        all_active_nodes = (node for owner, frontier in enumerate(frontier_list) if active[owner] for node in frontier)
        # TODO: We can do chunked processing with the list above. Simply grab a batch instead below.
        chunk_size = 1
        for nodes in split_seq(all_active_nodes, size=chunk_size):
            # TODO One can take the most promising node and then observe that the next nodes will always be less probable,
            #   so we can prune some nodes without exploring them with this principle.
            #frontier = new_frontier_list[owner]  # Get new frontier to place result in.
            owners, tokens, hiddens = [nodes[0].owner], nodes[0].token, nodes[0].hidden  # FIXME: Should be changed to a proper concatenation. (Stack tokens in dim 1!)
            tokens = F.one_hot(tokens.to(device=audio_padded.device), network.vocab_size).to(dtype=audio_padded.dtype)
            #print(tokens.shape, language_vectors.shape, language_vectors.dtype)
            tokens = network.add_language_vector(tokens, language_vectors[owners])
            # Tokens should be (len(tokens), 1, vocab_size+num_languages)
            if hiddens is not None:
                hiddens = hiddens[0].to(device=tokens.device), hiddens[1].to(device=tokens.device)

            decoder_state, decoder_hidden = network._decoder(tokens, one.expand(len(tokens)), h_n=hiddens)  # We add None axis to correspond to time.
            out = network._joint(encoder_state[owners, t, None], decoder_state) # Last index since it is only one time step.
            out = F.log_softmax(out[:, 0, 0], dim=-1)  # Dimension should be (node, vocab_size).
            def get_hiddens(hid):
                hid = hid[0].cpu(), hid[1].cpu()
                for i in range(hid[0].shape[1]):
                    yield hid[0][:, i, None], hid[1][:, i, None]

            # prob, pred = torch.max(out, dim=-1) this is in case of greedy search.
            for node, out, hidden in zip(nodes, out.cpu(), get_hiddens(decoder_hidden)):
                new_frontier = new_frontier_list[node.owner]  # Grab by reference. (Multiple nodes can have this owner.)
                for token, prob in enumerate(out):
                    new_node = BeamSearchNode(log_prob=prob.item(),
                                              token=torch.empty((1, 1), dtype=torch.int64).fill_(token), hidden=hidden, parent=node)
                    #if new_node.length > max_length:  # This length-limiting will not work as we lose the frontier then.
                    #   continue

                    score = new_node.score()
                    code = network.text_transformer.decode(torch.LongTensor(new_node.recurse()))
                    if code == 'det a' or code == 'det aaaaeet':
                        #print('YAS2', score, code)
                        None
                    # score = prob and width = 1 should give same results as the greedy decoder.
                    if len(new_frontier) < width:
                        heapq.heappush(new_frontier, (score, new_node))
                    else:
                        heapq.heappushpop(new_frontier, (score, new_node))
                #node.hidden = None  # Allow memory to be freed. (In any case it can be reconstructed since we have the prefixing sequence.)

        #frontier_list = list(map(lambda candidates: list(map(lambda candidate: candidate[1], candidates)), new_frontier_list))  # Remove the scores.
        for idx, lst in enumerate(new_frontier_list):
            if lst:
                # Move frontier
                frontier_list[idx] = [node for score, node in lst]
                lst.clear()
        #print(frontier_list[0])
    best_results = [frontier[0] for frontier in frontier_list] # Since they came from heapq's with the most probable item first.
    best_results = map(lambda result: result.recurse(), best_results)
    best_results = map(lambda result: network.text_transformer.decode(torch.LongTensor(result)), best_results)
    best_results = list(best_results)
   # FIXME: Now traverse the paths to generate the final strings.
    return best_results



if __name__ == "__main__":
    torch.backends.cudnn.enabled = False  # Much slower, but removes the errors.
    #model_path = os.path.join('.', 'lightning_logs', 'version_121')
    model_path = sys.argv[1]
    model_path = newest(os.path.join(model_path, 'checkpoints'))
    print('Loading model:', model_path)
    print(len(sys.argv))
    if len(sys.argv) > 2:
        checkpoint = torch.load(model_path, map_location=lambda storage, loc: storage)
        try:
            ckpt_hparams = checkpoint['hparams']
        except KeyError:
            raise IOError(
                "Checkpoint does not contain hyperparameters. Are your model hyperparameters stored"
                "in self.hparams?"
            )
        hparams = Namespace(**ckpt_hparams)
        if sys.argv[2] == 'fr':
            hparams.languages = ['fr']
        elif sys.argv[2] == 'it':
            hparams.languages = ['it']
        elif sys.argv[2] == 'multilingual':
            hparams.languages = ['fr', 'it']
        #hparams.batch_size = 32
        # load the state_dict on the model automatically
        model = Transducer(hparams)
        try:
            model.load_state_dict(checkpoint['state_dict'])
        except RuntimeError:
            print('Loaded network does not match. (Happens in case we go from unilingual to multilingual)')
            print('Trying to cast weights.')
            def select_subset(smaller):
                return tuple(slice(None, k, None) for k in smaller.shape)
            state_dict = model.state_dict()
            pretrained_dict = {k: v for k, v in checkpoint['state_dict'].items() if k in state_dict}
            for k in pretrained_dict:
                print('Copying', k, 'from', pretrained_dict[k].shape, 'to', state_dict[k].shape)
                state_dict[k][select_subset(pretrained_dict[k])] = pretrained_dict[k]
            model.load_state_dict(state_dict)
    else:
        # Code to load model from a file.
        model = Transducer.load_from_checkpoint(model_path)
        #model: Transducer = Transducer.load_from_metrics(
        #    weights_path=newest(os.path.join(model_path)),
        #    tags_csv=os.path.join(model_path, '..', 'meta_tags.csv'),
        #)

    model.summarize('full')
    
    kwargs = {}
    if model.hparams.gradients_clip: # Probably not needed anymore (was for backwards compatability.)
        kwargs['gradient_clip_val'] = model.hparams.gradients_clip

    trainer = pl.Trainer(gpus=1, track_grad_norm=2, weights_summary='full', **kwargs)

    trainer.fit(model)
    #trainer.test(model)