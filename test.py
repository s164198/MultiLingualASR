import os
from argparse import Namespace

import pytorch_lightning as pl

from model import Transducer
import sys
import numpy as np

def newest(path):
    files = os.listdir(path)
    paths = [os.path.join(path, basename) for basename in files]
    return max(paths, key=os.path.getctime)

import torch

if __name__ == "__main__":
    torch.backends.cudnn.enabled = False  # Much slower, but removes the errors.
    #model_path = os.path.join('.', 'lightning_logs', 'version_121')
    model_path = sys.argv[1]
    model_path = newest(os.path.join(model_path, 'checkpoints'))

    model = Transducer.load_from_checkpoint(model_path)

    kwargs = {}
    if model.hparams.gradients_clip: # Probably not needed anymore (was for backwards compatability.)
        kwargs['gradient_clip_val'] = model.hparams.gradients_clip

    trainer = pl.Trainer(gpus=1, **kwargs)

    trainer.test(model)