import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn

import pytorch_lightning as pl
from rnntloss import RNNTLoss
import os
from jiwer import wer
from beam_decoder import beam_search
from alpha_beta import compute_alpha, compute_beta
import matplotlib.pyplot as plt
import editdistance
from beam_decode_new import beam_search as beam_search_new

class Encoder(nn.Module):
    def __init__(self, input_size, hidden_size=1024, num_layers=8, projection=640, adapter=256):
        super().__init__()
        # Paper: hidden_size=2048, 640 projection layer, adapter modules with 256 dim bottleneck.
        # Residual adapters to come. Maybe.
        self._lstm = nn.LSTM(input_size=input_size,
                             hidden_size=hidden_size, num_layers=num_layers, batch_first=True)
        self.output_size = hidden_size
        # We can allow the initial hidden state to be learned. (Put these in the Transducer?)
        # self._init_hidden = nn.Parameter(torch.zeros(1, 1, hidden_size * self.num_directions))
        # self._init_cell_state = nn.Parameter(torch.zeros(1, 1, hidden_size * self.num_directions))
        # self._lstm.flatten_parameters()  # Should be done?

    def forward(self, inputs, inputs_len, h_n=None):
        inputs = rnn.pack_padded_sequence(inputs, inputs_len, batch_first=True, enforce_sorted=False)
        output, h_n = self._lstm(inputs, h_n)
        output, _ = rnn.pad_packed_sequence(output, batch_first=True)
        return output, h_n

    def get_expected_hidden_size(self, *args, **kwargs):
        return self._lstm.get_expected_hidden_size(*args, **kwargs)


class PredictionNetwork(nn.Module):
    # Also called the Decode Network. (PredNet)
    # TODO: Rename all references to prediction. I always forget what decoder means.

    def __init__(self, input_size, hidden_size=1024, num_layers=2):
        super().__init__()
        # Paper: hidden_size=2048, 640 projection.
        # TODO: Projection layers between LSTMs in this and Encoder.
        self._lstm = nn.LSTM(input_size=input_size,
                             hidden_size=hidden_size, num_layers=num_layers, batch_first=True)
        self.output_size = hidden_size

    def forward(self, inputs, inputs_len, h_n=None):
        inputs = rnn.pack_padded_sequence(inputs, inputs_len, batch_first=True, enforce_sorted=False)
        output, h_n = self._lstm(inputs, h_n)
        output, _ = rnn.pad_packed_sequence(output, batch_first=True)
        return output, h_n

    def get_expected_hidden_size(self, *args, **kwargs):
        return self._lstm.get_expected_hidden_size(*args, **kwargs)


class Joint(nn.Module):
    # https://github.com/ZhengkunTian/rnn-transducer/blob/master/rnnt/model.py

    def __init__(self, encoder_out_size, decoder_out_size, vocab_size, hidden_size=640, concat=False, num_layers=1):
        super().__init__()
        if concat:
            input_size = encoder_out_size + decoder_out_size
        else:
            assert encoder_out_size
            input_size = encoder_out_size

        self.projection = nn.Linear(input_size, hidden_size)
        self.activation = nn.ReLU()
        self.to_vocab = nn.Linear(hidden_size, vocab_size)
        self.to_vocab_2 = nn.Linear(hidden_size, vocab_size, bias=False)  # TODO: Delete me?
        self.concat = concat
        self.num_layers = num_layers

    @staticmethod
    def combine_broadcast(encoder_out, decoder_out, combine_fn=lambda x, y: torch.cat([x, y], -1)):
        # Allows to broadcast a concatenation.
        # Broadcast
        finalshape = tuple(map(max, zip(encoder_out.shape, decoder_out.shape)))
        encoder_out = encoder_out.expand(finalshape)
        decoder_out = decoder_out.expand(*finalshape)
        return combine_fn(encoder_out, decoder_out)

    def forward(self, audio_feat, prednet_feat):
        # BIG MEMORY SINK
        # Output must be (batch x seqLength x labelLength x outputDim), where seqLength is length of audio.
        prednet_feat = prednet_feat.unsqueeze(1)  # Prediction network (decoder) output, (batch x 1 x labelLength x ...)
        audio_feat = audio_feat.unsqueeze(2)  # Encoder (audio) output, (batch x seqLength x 1 x ...)

        if self.num_layers == 0:
            return self.to_vocab(audio_feat) + self.to_vocab_2(prednet_feat)

        if not self.concat:
            # In this case we are just adding them, by linearity this can be optimized:
            x = self.projection(audio_feat) + self.projection(prednet_feat)
            # Same as x = self.projection(self.combine_broadcast(audio_feat, prednet_feat, torch.add))
        else:
            x = self.projection(Joint.combine_broadcast(audio_feat, prednet_feat))
        x = self.activation(x)
        return self.to_vocab(x)


from PadTextAudio import PadTextAudio
from AudioTransformer import AudioTransformer
from TextTransformer import TextTransformer
from common_voice_dataset import split_dataset, CommonVoiceDataset, ConcatSet
import torch.utils.data as data
from argparse import ArgumentParser


class Transducer(pl.LightningModule):

    @staticmethod
    def add_model_specific_args(parent_parser, root_dir):
        """
        Specify the hyperparams for this LightningModule
        """
        # MODEL specific
        parser = ArgumentParser(parents=[parent_parser])
        parser.add_argument('--languages', default='sv-SE', type=str, help='languages to train on.', nargs='+')
        parser.add_argument('--batch-size', default=32, type=int, help='batch_size')
        parser.add_argument('--lr', default=1e-4, type=float, help='initial learning rate')
        parser.add_argument('--concat', default=False, type=bool,
                            help='should the joint grid be made of the concatenation of encoder and decoder output? '
                                 'Default is to broadcast add, and not concatenate.')
        parser.add_argument('--hidden-size', default=256, type=int, help='number of hidden units')  # 1024
        parser.add_argument('--normalize-loss', default=True, type=bool,
                            help='if the loss of a sequence should be normalize with respect its length')
        parser.add_argument('--num-workers', default=0, type=int,
                            help='number of hidden units. Set to -1 to use os.cpu_count() workers')
        parser.add_argument('--num-joint-hidden', default=1, type=int,  # FIXME: values >1 not implemented yet.
                            help='number of hidden layers in the joint network')
        parser.add_argument('--joint-factor', default=1, type=float,
                            help='the factor to multiply hidden-size to get the join space hidden size')
        parser.add_argument('--no-encoder', action='store_true',
                            help='if only the encoder network should be used')
        parser.add_argument('--bn-audio', action='store_true',
                            help='if batch norm should be applied to the audio')
        parser.add_argument('--no-decoder', action='store_true',
                            help='if only the encoder network should be used')
        parser.add_argument('--log-level', default=1, type=int,
                            help='the level of debugging logs to save (mainly tensorboard logs)')
        parser.add_argument('--beam-width', default=1, type=int,
                            help='the width to use in beam decoder for doing inference')  # FIXME: Implement
        parser.add_argument('--max-audio-length', default=200, type=int,
                            help='the width to use in beam decoder for doing inference')

        # TODO Create an argument of where to save/load the model to/from?
        return parser

    def __init__(self, hparams):
        super().__init__()
        self.hparams = hparams  # The hyperparameter in PyTorch-Lightning is not too well developed it seems.
        try:
            self.loss = RNNTLoss(backend=1, reduction='none')
        except ModuleNotFoundError:
            # Fall back on the other backend.
            try:
                self.loss = RNNTLoss(backend=0, reduction='none')
            except ModuleNotFoundError:
                self.loss = None

        num_workers = self.hparams.num_workers
        if num_workers is -1:
            num_workers = os.cpu_count()

        self.audio_transformer = AudioTransformer()
        self.text_transformer = TextTransformer()  # FIXME: Add alphabet (If we use this)
        collate_transformer = PadTextAudio()

        # Create individual datasets
        self.num_languages = len(self.hparams.languages)
        ds = [CommonVoiceDataset(".", lang, idx=i, audio_transformer=self.audio_transformer,
                                 max_length=self.hparams.max_audio_length, num_workers=num_workers,
                                 text_transformer=self.text_transformer) for i, lang in enumerate(self.hparams.languages)]

        splits = [split_dataset(d) for d in ds]

        # Concat train, val and test
        train_set = ConcatSet([s[0] for s in splits])
        val_set = ConcatSet([s[1] for s in splits])
        test_set = ConcatSet([s[2] for s in splits])
        print('Dataset size: ', len(test_set)+ len(val_set) + len(train_set))
        # Create corresponding loaders
        self.train_loader = data.DataLoader(train_set, batch_size=self.hparams.batch_size, shuffle=True,
                                            pin_memory=True,
                                            collate_fn=collate_transformer, num_workers=num_workers)
        self.val_loader = data.DataLoader(val_set, batch_size=self.hparams.batch_size,
                                          collate_fn=collate_transformer, num_workers=num_workers)
        self.test_loader = data.DataLoader(test_set, batch_size=self.hparams.batch_size,
                                           collate_fn=collate_transformer, num_workers=num_workers)

        if False:
            # Overfitting test (Trains on just two samples from the same set)
            self.train_loader = data.DataLoader(data.Subset(train, range(1)), batch_size=self.hparams.batch_size,
                                                shuffle=True, pin_memory=True, collate_fn=collate_transformer, num_workers=num_workers)
            self.val_loader = data.DataLoader(data.Subset(train, range(1)), batch_size=1,
                                              collate_fn=collate_transformer, num_workers=num_workers)

        # Languages
        # Vocabulary (can be derived from the languages?)
        self.vocab_size = len(self.text_transformer)  # == len(ds.alphabet)
        audio_features = self.audio_transformer.n_mfcc * self.audio_transformer.stack
        print('Alphabet: ', ''.join(sorted(self.text_transformer.alphabet)), self.vocab_size, self.num_languages)

        self._encoder = Encoder(audio_features + self.num_languages,
                                hidden_size=self.hparams.hidden_size)  # Add num_languages for language vectors.
        self._decoder = PredictionNetwork(self.vocab_size + self.num_languages,
                                          hidden_size=self.hparams.hidden_size)  # FIXME: Add 1 for the blank. But this symbol is unused. Is it an issue?

        self._joint = Joint(self._encoder.output_size, self._decoder.output_size,
                            self.vocab_size, concat=self.hparams.concat,
                            hidden_size=int(self.hparams.hidden_size * self.hparams.joint_factor),
                            num_layers=self.hparams.num_joint_hidden)

        if self.hparams.bn_audio:
            self.bn = nn.BatchNorm1d(num_features=audio_features, affine=False)

        self.log_level = self.hparams.log_level  # FIXME: Should be a parameter

    def get_prediction_hidden_size(self, *args, **kwargs):
        return self._decoder.get_expected_hidden_size(*args, **kwargs)

    @staticmethod
    def add_language_vector(tensors, language_vectors):
        # Broadcast shape.
        language_vectors = language_vectors[:, None].to(dtype=tensors.dtype).expand(-1, tensors.shape[1], -1)
        return torch.cat([tensors, language_vectors], -1)

    def forward(self, audio_padded, text_padded, audio_lengths, text_lengths, languages, h_n=(None, None)):
        encoder_h_n, decoder_h_n = h_n

        if self.hparams.bn_audio:
            audio_padded = self.bn(audio_padded.reshape(-1, audio_padded.shape[-1])).reshape(*audio_padded.shape)

        prediction_input = F.one_hot(text_padded.long(), self.vocab_size).to(dtype=audio_padded.dtype)  # Convert to float tensor
        # FIXME: Handle .eval correctly.

        # Add language vectors
        language_vectors = F.one_hot(languages, self.num_languages)
        prediction_input = self.add_language_vector(prediction_input, language_vectors)
        encoder_input = self.add_language_vector(audio_padded, language_vectors)

        # Audio in encoder network.
        encoder_state, encoder_h_n = self._encoder(encoder_input, audio_lengths, encoder_h_n)
        # The returned values are in the original order.
        if self.hparams.no_encoder:
            encoder_state = torch.zeros_like(encoder_state)

        # Text in prediction network.
        prednet_state, decoder_h_n = self._decoder(prediction_input, text_lengths, decoder_h_n)
        if self.hparams.no_decoder:
            prednet_state = torch.zeros_like(prednet_state)
            #prednet_state = audio_padded.new_zeros(prediction_input.shape[:-1] + (self._decoder.output_size,))
            #hidden_size = self._decoder.get_expected_hidden_size(None, (len(audio_padded),))
            #prednet_state, decoder_h_n = None, torch.zeros(hidden_size)
        logits = self._joint(encoder_state, prednet_state)

        return logits, (encoder_h_n, decoder_h_n)

    def batch_loss(self, audio_padded, text_padded, audio_lengths, text_lengths, languages):
        # We append 0 in start of 2nd dim (seq dim), as a blank initial input.
        # logits, _ = self.forward(audio_padded, F.pad(text_padded, [1, 0, 0, 0]),
        # Padding is specified in reverse order for some reason.
        #                         audio_lengths, text_lengths + 1,
        #                         languages)  # Could also use self.text_transformer.encode

        logits, _ = self.forward(audio_padded, F.pad(text_padded, [1, 0, 0, 0]),
                                 # Padding is specified in reverse order for some reason.
                                 audio_lengths, text_lengths + 1,
                                 languages)  # Could also use self.text_transformer.encode
        loss = self.loss(logits, text_padded.int(), audio_lengths.int(), text_lengths.int())

        # We normalize loss with respect to text length.
        if self.hparams.normalize_loss:
            loss = loss / text_lengths.to(loss.dtype)
        return loss

    def batch_prediction(self, audio_padded, audio_lengths, languages):
        # Source: https://github.com/ZhengkunTian/rnn-transducer/blob/master/rnnt/search.py

        language_vectors = F.one_hot(languages, self.num_languages)

        encoder_input = self.add_language_vector(audio_padded, language_vectors)

        # The encoding of audio can be done in one step. (No need to track hidden state then)
        encoder_state, _ = self._encoder(encoder_input, audio_lengths)

        # Start with the blank symbol. (Like F.pad in self.batch_loss)
        zero_token = torch.zeros((1, 1), dtype=torch.int64, device=audio_padded.device)
        zero_token = F.one_hot(zero_token, self.vocab_size).to(dtype=audio_padded.dtype)  # One-hot input
        one = torch.ones((1,), dtype=torch.int32, device=audio_padded.device)  # Sequence length.

        def decode(inputs, lengths, lang_vec):
            log_prob = 0
            token_list = []
            decoder_state, decoder_h_n = self._decoder(self.add_language_vector(zero_token, lang_vec),
                                                       one)
            for t in range(lengths):  # Every audio sample can potentially result in a symbol prediction.
                out = self._joint(inputs[:, t, None], decoder_state)[:, 0, :]
                out = F.log_softmax(out, dim=-1)
                prob, pred = torch.max(out, dim=-1)
                log_prob += prob.item()
                if pred:  # Check if output is all blank - encoded as zero.
                    token_list.append(pred.item())
                    # if len(token_list) == len('det h'):
                    #    print('YAS', log_prob, self.text_transformer.decode(torch.LongTensor(token_list)))
                    token = F.one_hot(pred.long(), self.vocab_size).to(dtype=audio_padded.dtype)
                    decoder_state, decoder_h_n = self._decoder(self.add_language_vector(token, lang_vec),
                                                               one,
                                                               h_n=decoder_h_n)
                # if len(token_list) == len('det har'):
                #    print('gucci', log_prob)
            return torch.LongTensor(token_list)

        results = []
        for i in range(audio_padded.shape[0]):
            decoded_seq = decode(encoder_state[i, None], audio_lengths[i, None], language_vectors[i, None])
            results.append(decoded_seq)

        results = list(map(self.text_transformer.decode, results))
        return results

    def training_step(self, batch, batch_nb):
        loss = self.batch_loss(*batch).mean(0)

        if batch_nb == 0 and self.log_level > 0: # Lets just make one graph.
            self.log_batch(f'train_{batch_nb}', batch, self.trainer.current_epoch)
            _, _, audio_lengths, text_lengths, _ = batch
            self.logger.experiment.add_scalar('train_max_audio', max(list(audio_lengths)), self.trainer.current_epoch)
            self.logger.experiment.add_scalar('train_max_text', max(list(text_lengths)), self.trainer.current_epoch)

        tensorboard_logs = {'train_loss': loss}
        return {'loss': tensorboard_logs['train_loss'], 'log': tensorboard_logs, 'progress_bar': tensorboard_logs}

    @pl.data_loader
    def train_dataloader(self):
        return self.train_loader

    @pl.data_loader
    def val_dataloader(self):
        return self.val_loader

    @pl.data_loader
    def test_dataloader(self):
        return self.test_loader

    def log_batch(self, tag, batch, step):
        # TODO: Maybe move this outside of the to a separate file.
        import numpy as np
        audio_padded, text_padded, audio_lengths, text_lengths, languages = batch

        with torch.no_grad():
            logits, _ = self.forward(audio_padded[:1], F.pad(text_padded[:1], [1, 0, 0, 0]),
                                     # Padding is specified in reverse order for some reason.
                                     audio_lengths[:1], text_lengths[:1] + 1,
                                     languages[:1])  # Could also use self.text_transformer.encode

        # Compute alpha-beta
        def alpha_beta(logit, target_text):
            logP = F.log_softmax(logit, -1)
            alphas = compute_alpha(target_text, logP.cpu().numpy())
            betas = compute_beta(target_text, logP.cpu().numpy())
            return alphas, betas

        def plot_alpha_beta(alphas, betas):
            alphas_betas = alphas + betas  # Because we are in log-space
            fig, ax = plt.subplots(1, 4, squeeze=True, sharey=True, sharex=True,
                                   figsize=(6.4*2, 4.8*2), constrained_layout=True)
            ax[0].imshow(alphas, cmap='jet', interpolation='nearest', aspect='auto')
            ax[0].set_title('Forward')
            ax[1].imshow(betas, cmap='jet', interpolation='nearest', aspect='auto')
            ax[1].set_title('Backward')
            ax[2].imshow(alphas_betas, cmap='jet', interpolation='nearest', aspect='auto')
            ax[2].set_title('log(Prob)')
            ax[3].imshow(np.exp(alphas_betas), cmap='jet', interpolation='nearest', vmin=0, aspect='auto')
            ax[3].set_title('Prob')
            ax[0].set_ylabel('t')
            ax[0].invert_yaxis()
            for axx in ax:
                axx.set_xlabel('u')
            for axx in ax[1:]:
                axx.get_yaxis().set_visible(False)
            fig.suptitle('RNN-T loss', fontsize=16)
            return fig

        def plot_log_prob(network, audio_input, target_text, alphas, betas):
            fig, ax = plt.subplots(2, 1, sharex=False, figsize=(6.4, 4.8), constrained_layout=True,
                                   gridspec_kw={'height_ratios': [3, 1]})
            extent = [-0.5, alphas.shape[0] - 0.5, -0.5, alphas.shape[1] - 0.5]
            ax[0].imshow(np.rot90(np.exp(alphas + betas)), cmap='jet',
                         interpolation='nearest', extent=extent, aspect='auto')
            ax[0].set_title('Input vs log(Prob)')
            ax[0].invert_yaxis()
            text_ax = ax[0].twinx()
            text_ax.set_yticks(np.array(range(alphas.shape[-1])) - 0.5)
            text_ax.set_yticks(np.array(range(alphas.shape[-1])), minor=True)
            # ax.tick_params(axis='x', which='minor', bottom=False)
            text_ax.set_yticklabels(target_text, rotation=-90, rotation_mode='anchor',
                                    ha='center', va='baseline', minor=True, in_layout=False)
            text_ax.yaxis.set_tick_params(which='minor', length=0)
            text_ax.grid(True, axis='y', linestyle='-', color='k')
            text_ax.set_yticklabels([''] * alphas.shape[-1], minor=False)
            text_ax.set_ylim(*ax[0].get_ylim())  # Also inverts this axis
            import librosa
            import librosa.display
            mfccs = audio_input.reshape(-1, 80).cpu().T.numpy()
            librosa.display.specshow(mfccs, x_axis='time', sr=1,
                                     hop_length=network.audio_transformer.hop_time * network.audio_transformer.hop / network.audio_transformer.stack,
                                     ax=ax[1])
            ax[1].yaxis.set_label_position("right")
            ax[1].set_ylabel('MFCC')
            # fig.suptitle('Input vs log(Prob)', fontsize=16)
            return fig

        # window_stack(torch.arange(640+7), width=self.audio_transformer.stack).reshape(-1, self.audio_transformer.stack * self.audio_transformer.n_mfcc)[::3].reshape(-1, self.audio_transformer.n_mfcc)[:, 0]

        # PREDICTION TEXTS
        ground_truth_text = (text_padded[i, :length] for i, length in enumerate(text_lengths))
        ground_truth_text = list(map(self.text_transformer.decode, ground_truth_text))
        predicted_text = self.batch_prediction(audio_padded[:1], audio_lengths[:1], languages[:1])
        predicted_text_beam = beam_search(self, audio_padded[:1], audio_lengths[:1], languages[:1], width=20)
        self.logger.experiment.add_text(f'{tag}_prediction',
                                        'ground truth: "{}", prediction: "{}", beam prediction:"{}"'.format(
                                            ground_truth_text[0], predicted_text[0], predicted_text_beam[0]),
                                        global_step=step)
        # PLOT OF ALPHA BETAS
        alphas, betas = alpha_beta(logits[0, :audio_lengths[0], :text_lengths[0]], text_padded[0, :text_lengths[0]])
        fig = plot_alpha_beta(alphas, betas)
        self.logger.experiment.add_figure(f'{tag}_alphas_betas', fig, global_step=step,
                                          close=True)

        predicted_text_beam_tensor = self.text_transformer.encode(predicted_text_beam[0])
        if len(predicted_text_beam_tensor) != 0 and len(predicted_text_beam_tensor) <= logits.shape[1]:
            fig = plot_alpha_beta(*alpha_beta(logits[0, :audio_lengths[0], :len(predicted_text_beam_tensor)],
                                              predicted_text_beam_tensor))
            self.logger.experiment.add_figure(f'{tag}_alphas_betas_BEAMPRED', fig, global_step=step,
                                              close=True)

        # PLOT OF Log Prob and MFCCS
        fig = plot_log_prob(self, audio_padded[0, :audio_lengths[0]],
                            ground_truth_text[0], alphas, betas)
        self.logger.experiment.add_figure(f'{tag}_alpha_betas_MFCC', fig, global_step=step,
                                          close=True)

        # PLOT OF NON-ZERO PROB
        # Make image of probability of blank vs non blank.
        nonblank_prob = 1 - torch.softmax(logits, -1)[..., 0, None].expand(-1, -1, -1, 3)
        self.logger.experiment.add_images(f'{tag}_nonblank_prop', nonblank_prob, dataformats='NHWC',
                                          global_step=step)

        # TODO: Add text sample of prediction.
        # loss = self.loss(logits, text_padded.int(), audio_lengths.int(), text_lengths.int())
        # self.logger (batch[0][0, :batch[2][0]])

    def validation_step(self, batch, batch_nb):
        val_loss = self.batch_loss(*batch).cpu()
        # PERFORM INFERENCE
        audio_padded, text_padded, audio_lengths, text_lengths, languages = batch
        ground_truth_text = (text_padded[i, :length] for i, length in enumerate(text_lengths))
        ground_truth_text = list(map(self.text_transformer.decode, ground_truth_text))

        # TODO: Implement beam search with single batch. Then extend.

        from train_checkpoint import beam_decode

        # predicted_text_beam = beam_decode(self, audio_padded[:1], audio_lengths[:1], languages[:1])
        # predicted_text_beam_new = beam_search(self, audio_padded[:1], audio_lengths[:1], languages[:1])

        #print('gt{}', ground_truth_text[0])
        #print('pred', predicted_text[0])
        #print('beam', predicted_text_beam[0])
        #print('beam_new', predicted_text_beam_new[0])

        #for k in range(1, audio_padded.shape[0]):
        #    predicted_text_beam_2 = beam_decode(self, audio_padded[k:k+1], audio_lengths[k:k+1], languages[k:k+1])
        #    print('gt: "{}", beam: "{}"'.format(ground_truth_text[k], predicted_text_beam_2[0]))
        if self.trainer.current_epoch > 2:
            predicted_text_beam = beam_search(self, audio_padded, audio_lengths, languages, width=self.hparams.beam_width)
            word_error_rate = [wer(gt, pred) for gt, pred in zip(ground_truth_text, predicted_text_beam)]
        else:
            word_error_rate = []

        if batch_nb == 0 and self.log_level > 0: # Lets just make one graph.
            audio = audio_padded[0, :audio_lengths[0]]
            if self.hparams.bn_audio:
                audio = self.bn(audio.reshape(-1, audio.shape[-1])).reshape(*audio.shape)
            for t in range(len(audio)):
                self.logger.experiment.add_histogram('val_hist_audio', audio[t], global_step=t)

            if self.trainer.current_epoch == 0: # First time validation is run.
                if False:
                    self.logger.experiment.add_graph(self, input_to_model=(audio_padded[:1],
                                                                           text_padded[:1],
                                                                           audio_lengths[:1],
                                                                           text_lengths[:1],
                                                                           languages[:1]))
                    #  self.logger.experiment.flush()  # Graph wont show github.com/pytorch/pytorch/issues/24157

            self.log_batch(f'val_{batch_nb}', batch, self.trainer.current_epoch)




        return {'val_loss': val_loss, 'wer': word_error_rate}  # Move loss to CPU so we don't use too much GPU memory.

    # if you have one test dataloader:
    def test_step(self, batch, batch_nb):
        W = self.hparams.beam_width
        assert(W == 1)
        audio_padded, text_padded, audio_lengths, text_lengths, languages = batch
        ground_truth_text = (text_padded[i, :length] for i, length in enumerate(text_lengths))
        ground_truth_text = list(map(self.text_transformer.decode, ground_truth_text))

        predicted_text_beam = []
        language_vectors = F.one_hot(languages, self.num_languages)
        if self.hparams.bn_audio:
            audio_padded = self.bn(audio_padded.reshape(-1, audio_padded.shape[-1])).reshape(*audio_padded.shape)
        encoder_state, _ = self._encoder(self.add_language_vector(audio_padded, language_vectors), audio_lengths)
        with torch.no_grad():
            batch_size = len(audio_padded)
            for i in range(batch_size):
                bk, bp = beam_search_new(self, encoder_state[i, :audio_lengths[i]],
                                     language_vectors[i, None], W=W, prefix=False)
                pred = self.text_transformer.decode(bk[1:])
                if self.hparams.log_level > 1 and False:
                    print('Original:', ground_truth_text[i])
                    print('Transcription: ', pred)
                    print('GER:',
                          "{0:.0%}".format(editdistance.eval(ground_truth_text[i], pred)/len(ground_truth_text[i])),
                          'WER:', "{0:.0%}".format(wer(ground_truth_text[i], pred)))
                predicted_text_beam.append(pred)

        for audio_padded_s, audio_lengths_s, languages_s in zip(audio_padded, audio_lengths, languages):
            #from test import beam_decode
            #predicted_text_beam.append(beam_decode(self, audio_padded_s[None], audio_lengths_s[None],
            #                                  languages_s[None], width=self.hparams.beam_width)) #FIXME: There shouldn't be None on languages!? or audio lengths
            #print(editdistance.eval(gt, pred) / len(gt), gt, ':', pred, bp, wer(gt, pred), )
            pass

        word_error_rates = [(wer(gt, pred) * len(gt.split()), len(gt.split())) for gt, pred in zip(ground_truth_text, predicted_text_beam)]
        grapheme_error_rates = [(editdistance.eval(gt, pred), len(gt)) for gt, pred in zip(ground_truth_text, predicted_text_beam)]

        loss = self.batch_loss(*batch).cpu()
        #for i in range(len(word_error_rates)):
        #    print('Test:', i)
        #    print(ground_truth_text[i])
        #    print(predicted_text_beam[i])
        #    print(word_error_rates[i])

        return {'wer': word_error_rates, 'ger': grapheme_error_rates, 'test_loss': loss, 'language': languages.cpu()}

    def test_end(self, outputs):
        # OPTIONAL
        wer = 0
        wer_cnt = 0
        ger = 0
        ger_cnt = 0
        for x in outputs:
            if 'wer' in x:
                for i in range(len(x['wer'])):
                    wer += x['wer'][i][0]
                    wer_cnt += x['wer'][i][1]
                    ger += x['ger'][i][0]
                    ger_cnt += x['ger'][i][1]

        avg_loss = torch.cat([x['test_loss'] for x in outputs], 0).mean(0)

        tensorboard_logs = {'test_loss': avg_loss}
        if wer_cnt > 0:
            tensorboard_logs['wer'] = wer / wer_cnt
        if ger_cnt > 0:
            tensorboard_logs['ger'] = ger / ger_cnt
        if wer_cnt == 0 or wer_cnt == 0:
            print('No ger or wer?!')

        if True:
            wer = 0
            wer_cnt = 0
            ger = 0
            ger_cnt = 0
            for x in outputs:
                if 'wer' in x:
                    for i in range(len(x['wer'])):
                        if x['language'][i] == 0:
                            wer += x['wer'][i][0]
                            wer_cnt += x['wer'][i][1]
                            ger += x['ger'][i][0]
                            ger_cnt += x['ger'][i][1]
            if wer_cnt > 0:
                tensorboard_logs['wer0'] = wer / wer_cnt
            if ger_cnt > 0:
                tensorboard_logs['ger0'] = ger / ger_cnt

            wer = 0
            wer_cnt = 0
            ger = 0
            ger_cnt = 0
            for x in outputs:
                if 'wer' in x:
                    for i in range(len(x['wer'])):
                        if x['language'][i] == 1:
                            wer += x['wer'][i][0]
                            wer_cnt += x['wer'][i][1]
                            ger += x['ger'][i][0]
                            ger_cnt += x['ger'][i][1]
            if wer_cnt > 0:
                tensorboard_logs['wer1'] = wer / wer_cnt
            if ger_cnt > 0:
                tensorboard_logs['ger1'] = ger / ger_cnt

        return {'test_loss': tensorboard_logs['test_loss'], 'log': tensorboard_logs, 'progress_bar': tensorboard_logs}

    def validation_end(self, outputs):
        avg_loss = torch.cat([x['val_loss'] for x in outputs], 0).mean(0)
        wer = 0
        instances = 0
        for x in outputs:
            if 'wer' in x:
                for error in x['wer']:
                    instances += 1
                    wer += error

        tensorboard_logs = {'val_loss': avg_loss}
        if instances > 0:
            tensorboard_logs['word_error_rate'] = wer / instances
        return {'val_loss': tensorboard_logs['val_loss'], 'log': tensorboard_logs, 'progress_bar': tensorboard_logs}

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.hparams.lr)
        #return torch.optim.SGD(self.parameters(), lr=self.hparams.lr * 1e-2, momentum=0.9)


if __name__ == "__main__":
    pass
