import torch

import heapq
import torch.nn.functional as F
import itertools
from dataclasses import dataclass, field
from typing import Any, Optional

def split_seq(iterable, size):
    """Yield successive size-sized chunks from the iterable."""
    # https://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks
    it = iter(iterable)
    item = list(itertools.islice(it, size))
    while item:
        yield item
        item = list(itertools.islice(it, size))

@dataclass
class BeamSearchNode:
    # FIELDS:
    log_prob: float
    token: float
    hidden: Optional[torch.Tensor]
    parent: Any  # Should be BeamSearchNode or None
    owner: int
    length: int

    @staticmethod
    def root(network, owner):
        # Middle dim of get_prediction_hidden_size is batch dim.
        hidden_size = network.get_prediction_hidden_size(None, (1,))
        return BeamSearchNode(log_prob=0, token=0, hidden=(torch.zeros(hidden_size), torch.zeros(hidden_size)),
                              parent=None, owner=owner, length=0)

    @staticmethod
    def new_node(log_prob, token, hidden, parent):
        if not token:  # If blank
            # In case token is blank the hidden state should not mutate.
            # (Since the output text-sequence has not advanced)
            hidden = parent.hidden
        return BeamSearchNode(log_prob=log_prob + parent.log_prob, token=token, hidden=hidden, owner=parent.owner,
                              length = parent.length + bool(token), parent=parent)

    def recurse(self):
        """ Returns the sequence of tokens that this node represents. """
        if self.parent is None:
            return []
        lst = self.parent.recurse()
        if self.token: # If not blank
            lst.append(self.token)
        return lst

    @property
    def heuristic(self):
        # The heapq is ordered with the smallest item as root.
        value = self.log_prob
        #if self.length > 0:
        #    value /= self.length  # To have a fair comparison we take the geometric average.
        return value


@dataclass(order=True)
class PrioritizedNode:
    item: BeamSearchNode = field(compare=False)
    priority: float = 0  # Maybe using 0 is sufficient.

    def __post_init__(self):
        self.priority = self.item.heuristic

def beam_search(network, audio_padded, audio_lengths: torch.IntTensor, languages,
                width=1, max_length=100):
    chunk_size = audio_padded.shape[0] # network.hparams.batch_size
    # Remember to turn off autograd when running this or you may get memory leaks. (This is intentional)
    language_vectors = F.one_hot(languages, network.num_languages)
    if network.hparams.bn_audio:
        audio_padded
    encoder_state, _ = network._encoder(network.add_language_vector(audio_padded, language_vectors), audio_lengths)

    one = audio_padded.new_ones((1,), dtype=torch.int32)  # We advance one token at a time.
    audio_lengths = audio_lengths.cpu()

    # The lengths of the lists in the active set never exceeds *width*.
    active_set = {batch_idx: [PrioritizedNode(BeamSearchNode.root(network, batch_idx))]
                  for batch_idx in range(len(audio_lengths))}
    closed_set = {}
    for t in range(audio_padded.shape[1]):  # For every step in audio...
        # CREATE FRONTIER (All nodes in the active set that must be expanded)
        frontier = []
        for batch_idx in list(active_set):
            #batch_idx, active_nodes = active_set.pop()
            if not (t < audio_lengths[batch_idx]):  # If the audio sequence is over for this batch.
                closed_set[batch_idx] = active_set.pop(batch_idx)
                continue
            # Take nodes and put in frontier.
            heap = active_set[batch_idx]  # (Discussed later why this is a heap.)
            frontier += map(lambda p_node:p_node.item, heap)  # Grab nodes
            heap.clear()

        # chunk_size = 1  # Chunk upload to GPU (Chunk size should be able to be as large as batch size)
        for nodes in split_seq(frontier, size=chunk_size):
            owners, tokens = [node.owner for node in nodes], [node.token for node in nodes]
            hiddens = torch.cat([node.hidden[0] for node in nodes], dim=1), \
                      torch.cat([node.hidden[1] for node in nodes], dim=1)  # Middle dim is batch dim in hidden state.

            # Prepare input
            hiddens = hiddens[0].to(device=audio_padded.device, non_blocking=True),\
                      hiddens[1].to(device=audio_padded.device, non_blocking=True)
            tokens = torch.LongTensor(tokens).to(device=audio_padded.device, non_blocking=True)
            tokens = F.one_hot(tokens[:, None], network.vocab_size).to(dtype=audio_padded.dtype)
            tokens = network.add_language_vector(tokens, language_vectors[owners])

            # Do predictions
            prediction_state, prediction_hidden = network._decoder(tokens, one.expand(len(tokens)), h_n=hiddens)
            pred = network._joint(encoder_state[owners, t, None], prediction_state)
            pred = F.log_softmax(pred[:, 0, 0], dim=-1)  # Dimension is then (len(nodes), network.vocab_size).
            pred, prediction_hidden = pred.cpu(), (prediction_hidden[0].cpu(), prediction_hidden[1].cpu())

            def get_hidden(node_idx):
                # Middle dim is batch dim in hidden state.
                return prediction_hidden[0][:, node_idx, None], prediction_hidden[1][:, node_idx, None]

            # Now compare the predictions and place in next active set.
            for node_idx, node in enumerate(nodes):
                # For every node in frontier we get a hidden state, and descendant nodes at tokens.
                hidden = get_hidden(node_idx)
                # A priority queue allows us to get the smallest element.
                # In this case we pop the element with the lowest probability, so we get the top *width* most probable.
                heap = active_set[node.owner]
                for token in range(pred.shape[-1]):
                    new_node = BeamSearchNode.new_node(log_prob=pred[node_idx, token].item(), token=token,
                                                       hidden=hidden, parent=node)
                    if len(heap) < width:
                        heapq.heappush(heap, PrioritizedNode(new_node))
                    else:
                        heapq.heappushpop(heap, PrioritizedNode(new_node))
                node.hidden = None  # Allow memory to be freed.
                # Done adding all children of this node.
            # Done creating the next active set by expanding the frontier nodes.
    # Done iterating over all of the audio sequence.
    closed_set.update(active_set)
    # Get the nodes for each set with the highest probability, and strip the probability value.
    def get_best_decoded(nodes):  # FIXME: nodes is unused!
        best = max(heap).item  # Get most probable node. Strip the score.
        return network.text_transformer.decode( best.recurse() )
    closed_set = [(batch_idx, get_best_decoded(nodes)) for batch_idx, nodes in closed_set.items()]
    closed_set = sorted(closed_set)  # Sort by batch_idx (These are then 1, 2, ..., len(audio_lengths) again.)
    _, closed_set = zip(*closed_set)
    return list(closed_set)




