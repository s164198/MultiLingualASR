import torch
from torch import autograd
import torch.nn.functional as F


class Sequence:
    def __init__(self, seq=None, blank=0):
        if seq is None:
            self.g = []  # predictions of grapheme language model
            self.k = [blank]  # prediction grapheme label
            # self.h = [None] # input hidden vector to grapheme model
            self.h = None
            self.logp = 0  # probability of this sequence, in log scale
        else:
            self.g = seq.g[:]  # save for prefixsum
            self.k = seq.k[:]
            self.h = seq.h
            self.logp = seq.logp


import math


def log_aplusb(a, b):
    return max(a, b) + math.log1p(math.exp(-math.fabs(a - b)))


def beam_search(rnnt, xs, W=10, prefix=False):
    """
    `xs`: acoustic model outputs
    NOTE only support one sequence (batch size = 1)
    """
    use_gpu = xs.is_cuda

    def forward_step(label, hidden):
        """ `label`: int """
        label = autograd.Variable(torch.LongTensor([label]), volatile=True).view(1, 1)
        if use_gpu: label = label.cuda()
        label = rnnt.embed(label)
        pred, hidden = rnnt._decoder(label, hidden)
        return pred[0][0], hidden

    def isprefix(a, b):
        # a is the prefix of b
        if a == b or len(a) >= len(b): return False
        for i in range(len(a)):
            if a[i] != b[i]: return False
        return True

    xs = rnnt._encoder(xs, [1])[0][0]
    B = [Sequence()]
    for i, x in enumerate(xs):
        sorted(B, key=lambda a: len(a.k), reverse=True)  # larger sequence first add
        A = B
        B = []
        if prefix:
            # for y in A:
            #     y.logp = log_aplusb(y.logp, prefixsum(y, A, x))
            for j in range(len(A) - 1):
                for i in range(j + 1, len(A)):
                    if not isprefix(A[i].k, A[j].k): continue
                    # A[i] -> A[j]
                    pred, _ = forward_step(A[i].k[-1], A[i].h) # FIXME
                    idx = len(A[i].k)
                    ytu = rnnt._joint(x, pred)
                    logp = F.log_softmax(ytu, dim=0)
                    curlogp = A[i].logp + float(logp[A[j].k[idx]])
                    for k in range(idx, len(A[j].k) - 1):
                        ytu = rnnt._joint(x, A[j].g[k])
                        logp = F.log_softmax(ytu, dim=0)
                        curlogp += float(logp[A[j].k[k + 1]])
                    A[j].logp = log_aplusb(A[j].logp, curlogp)

        while True:
            y_hat = max(A, key=lambda a: a.logp)
            # y* = most probable in A
            A.remove(y_hat)
            # calculate P(k|y_hat, t)
            # get last label and hidden state
            pred, hidden = forward_step(y_hat.k[-1], y_hat.h) #FIXME
            ytu = rnnt._joint(x, pred)
            logp = F.log_softmax(ytu, dim=0)  # log probability for each k
            # TODO only use topk vocab
            for k in range(rnnt.vocab_size):
                yk = Sequence(y_hat)
                yk.logp += float(logp[k])
                if k == rnnt.blank:
                    B.append(yk)  # next move
                    continue
                # store prediction distribution and last hidden state
                # yk.h.append(hidden); yk.k.append(k)
                yk.h = hidden
                yk.k.append(k)
                if prefix: yk.g.append(pred)
                A.append(yk)
            # sort A
            # sorted(A, key=lambda a: a.logp, reverse=True) # just need to calculate maximum seq

            # sort B
            # sorted(B, key=lambda a: a.logp, reverse=True)
            y_hat = max(A, key=lambda a: a.logp)
            yb = max(B, key=lambda a: a.logp)
            if len(B) >= W and yb.logp >= y_hat.logp: break

        # beam width
        sorted(B, key=lambda a: a.logp, reverse=True)
        B = B[:W]

    # return highest probability sequence
    print(B[0])
    return B[0].k, -B[0].logp
