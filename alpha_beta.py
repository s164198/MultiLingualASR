import numpy as np


def compute_alpha(text, logP, blank_idx=0):
    """
    text: list of int
    logP: (len(Audio)*len(Text)*len(alphabet)) logprobs
        t: index in audio input
        u: index in text output
        a: index in alphabet, (blank idx is 0 by default)
    """
    assert logP.shape != 3
    T, U, _ = logP.shape
    alphas = np.zeros((T, U))

    # Note base case handled by default as log(1) = 0
    # alpha(0, 0) = log(1)

    #  Limit case on the bottom, u=0
    for t in range(1, T):
        alphas[t, 0] = alphas[t - 1, 0] + logP[t - 1, 0, blank_idx]

    # Limit case on the left, t=0
    for u in range(1, U):
        alphas[0, u] = alphas[0, u - 1] + logP[0, u - 1, text[u - 1]]

    # General case
    for t in range(1, T):
        for u in range(1, U):
            logp_blank = alphas[t - 1, u] + logP[t - 1, u, blank_idx]  # alpha(t-1, u)*Ø(t-1, u)
            logp_char = alphas[t, u - 1] + logP[t, u - 1, text[u - 1]]  # alpha(t, u-1)*y(t, u-1)
            alphas[t, u] = np.logaddexp(logp_blank, logp_char)

    return alphas


def compute_beta(text, logP, blank_idx=0):
    """
    text: list of int
    logP: (len(Audio)*len(Text)*len(alphabet)) logprobs
        t: index in audio input
        u: index in text output
        a: index in alphabet, (blank idx is 0 by default)
    """
    assert logP.shape != 3
    T, U, _ = logP.shape
    betas = np.zeros((T, U))

    # Base case beta(T, U) = Ø(T, U)
    betas[T - 1, U - 1] = logP[T - 1, U - 1, blank_idx]

    #  Limit case on the top, u=U
    for t in reversed(range(T - 1)):
        betas[t, U - 1] = betas[t + 1, U - 1] + logP[t, U - 1, blank_idx]

    # Limit case on the left, t=T
    for u in reversed(range(U - 1)):
        betas[T - 1, u] = betas[T - 1, u + 1] + logP[T - 1, u, text[u]]

    # General case
    for t in reversed(range(T - 1)):
        for u in reversed(range(U - 1)):
            logp_blank = betas[t + 1, u] + logP[t, u, blank_idx]  # beta(t+1, u)*Ø(t, u)
            logp_char = betas[t, u + 1] + logP[t, u, text[u]]  # beta(t, u+1)*y(t, u)
            betas[t, u] = np.logaddexp(logp_blank, logp_char)

    return betas
