import librosa
import numpy as np
#import seaborn as sns
#sns.set(context="poster")
import matplotlib.pyplot as plt
from matplotlib import cm
import pytorch_lightning as pl
import librosa.display
from matplotlib.ticker import FixedLocator

from AudioTransformer import AudioTransformer, window_stack
from PadTextAudio import PadTextAudio
from TextTransformer import TextTransformer
from model import Transducer
from common_voice_dataset import CommonVoiceDataset, split_dataset, process_audio_entry
import torch
import torch.nn
import torch.nn.functional as F
from argparse import Namespace

import os, torch
path = os.path.join('.','pretrained','multilingual')

model = Transducer.load_from_checkpoint(os.path.join(path, 'checkpoints', '_ckpt_epoch_3.ckpt'))
#trainer = pl.Trainer()
#model.hparams.beam_width = 1
#trainer.test(model)
a4_dims = (11.7, 8.27)

audio_transformer = AudioTransformer()
text_transformer = TextTransformer()
collate_transformer = PadTextAudio()

text = 'this text is too long this text is too long this text is too long'
audio_file = 'common_voice_fr_18360183.mp3'
audio_path = os.path.join('.','common_voices','uncompressed','fr','clips')

text_translit = text_transformer.transliterate(text)
out = process_audio_entry(audio_text=(audio_file, text), root_path=audio_path,
                          audio_transformer=audio_transformer, text_transformer=text_transformer)
text_enc, mfcc = out['text'], out['audio']


print('Original text:', text)
print('Unicoded text:', text_translit)
print('Encoded text:', *map(lambda t:t.item(), text_enc))

audio, sr = librosa.load(os.path.join(audio_path, audio_file), None)
audio_trimmed, trim_idx = librosa.effects.trim(y=audio, top_db=audio_transformer.trim_db)

hop_length = int(audio_transformer.hop_time * sr)
window_length = int(audio_transformer.window_time * sr)

#rms = librosa.feature.rms(audio, frame_length=window_length, hop_length=hop_length).T

audio_padded, text_padded, audio_lengths, text_lengths, languages = collate_transformer([(mfcc, text_enc, 0)])

from beam_decode_new import beam_search
W = 1
model.eval()
language_vectors = F.one_hot(languages, model.num_languages)
if model.hparams.bn_audio:
    audio_padded = model.bn(audio_padded.reshape(-1, audio_padded.shape[-1])).reshape(*audio_padded.shape)
encoder_state, _ = model._encoder(model.add_language_vector(audio_padded, language_vectors), audio_lengths)
with torch.no_grad():
    bk, bp = beam_search(model, encoder_state[0], language_vectors[:1], W=W, prefix=False)
gt = text_transformer.decode(text_padded[0, :text_lengths[0]])
pred = text_transformer.decode(bk[1:])

print('RNN-T decoded text:', pred)

#plt.figure()
inactive_color = (0.5, 0.5, 0.5)
active_color = (153/255, 0, 0)
mfccvmin, mfccvmax, mfcccm = -100, 100, cm.jet

plt.title('Raw audio')
plt.plot(range(len(audio)), audio, label='Audio', color=inactive_color)
plt.plot(range(trim_idx[0], trim_idx[1]), audio[trim_idx[0]:trim_idx[1]], label='Audio (Trimmed)', color=active_color)
plt.ylabel('Amplitude')
plt.xlabel('Sample')
plt.legend()
plt.show()

n_fft = audio_transformer.next_power_of_2(window_length)

# Transform audio
from librosa.core.spectrum import power_to_db
import scipy
from librosa.feature.spectral import melspectrogram
S = power_to_db(melspectrogram(y=audio_trimmed, sr=sr, n_fft=n_fft, win_length=window_length, hop_length=hop_length))

M = scipy.fftpack.dct(S, axis=0, type=2, norm='ortho')
plt.pcolor(M, cmap=cm.gray, vmin=mfccvmin, vmax=mfccvmax)
plt.pcolor(M[:audio_transformer.n_mfcc], cmap=mfcccm, vmin=mfccvmin, vmax=mfccvmax)
#plt.legend()
plt.title('Feature extraction')
plt.ylabel("MFCC's")
plt.show()

plt.pcolor(M[:audio_transformer.n_mfcc], cmap=mfcccm, vmin=mfccvmin, vmax=mfccvmax)
#plt.legend()
plt.title('MFCC features')
plt.ylabel("mel-frequency")
plt.show()


from matplotlib import cm
ig, ax = plt.subplots()
cax = ax.imshow(mfcc.T, interpolation='nearest', cmap=mfcccm, origin='lower', aspect='auto', vmin=mfccvmin, vmax=mfccvmax)
ax.set_title('MFCC Input')
plt.ylabel('Feature')
plt.xlabel('Time (T)')
plt.show()


###### BIG NICE PLOT
from matplotlib.transforms import Bbox, TransformedBbox, blended_transform_factory
from mpl_toolkits.axes_grid1.inset_locator import BboxPatch, BboxConnector, BboxConnectorPatch

def connect_bbox(bbox1, bbox2,
                 loc1a, loc2a, loc1b, loc2b,
                 prop_lines, prop_patches=None):
    if prop_patches is None:
        prop_patches = {
            **prop_lines,
            "alpha": prop_lines.get("alpha", 1) * 0,
        }

    c1 = BboxConnector(bbox1, bbox2, loc1=loc1a, loc2=loc2a, **prop_lines)
    c1.set_clip_on(False)
    c2 = BboxConnector(bbox1, bbox2, loc1=loc1b, loc2=loc2b, **prop_lines)
    c2.set_clip_on(False)

    bbox_patch1 = BboxPatch(bbox1, **prop_patches)
    bbox_patch2 = BboxPatch(bbox2, **prop_patches)

    p = BboxConnectorPatch(bbox1, bbox2,
                           # loc1a=3, loc2a=2, loc1b=4, loc2b=1,
                           loc1a=loc1a, loc2a=loc2a, loc1b=loc1b, loc2b=loc2b,
                           **prop_patches)
    p.set_clip_on(False)

    return c1, c2, bbox_patch1, bbox_patch2, p


def zoom_effect01(ax1, ax2, xmin, xmax, xmin2, xmax2, ymin=0, ymax=1, ymin2=0, ymax2=1, **kwargs):
    """
    Connect *ax1* and *ax2*. The *xmin*-to-*xmax* range in both axes will
    be marked.

    Parameters
    ----------
    ax1
        The main axes.
    ax2
        The zoomed axes.
    xmin, xmax
        The limits of the colored area in both plot axes.
    **kwargs
        Arguments passed to the patch constructor.
    """

    trans1 = blended_transform_factory(ax1.transData, ax1.transAxes)
    trans2 = blended_transform_factory(ax2.transData, ax2.transAxes)

    bbox = Bbox.from_extents(xmin2, ymin2, xmax2, ymax2)

    mybbox1 = TransformedBbox(bbox, trans1)
    mybbox2 = TransformedBbox(Bbox.from_extents(xmin, ymin, xmax, ymax), trans2)

    prop_patches = {**kwargs, "ec": "none", "alpha": 0}

    c1, c2, bbox_patch1, bbox_patch2, p = connect_bbox(
        mybbox1, mybbox2,
        loc1a=3, loc2a=2, loc1b=4, loc2b=1,
        prop_lines=kwargs, prop_patches=prop_patches)

    ax1.add_patch(bbox_patch1)
    ax2.add_patch(bbox_patch2)
    ax2.add_patch(c1)
    ax2.add_patch(c2)
    ax2.add_patch(p)

    return c1, c2, bbox_patch1, bbox_patch2, p

fig, ax = plt.subplots(2, 1)

ax[0].set_title('Raw audio')
ax[0].plot(range(len(audio)), audio, label='Audio', color=inactive_color)
ax[0].plot(range(trim_idx[0], trim_idx[1]), audio[trim_idx[0]:trim_idx[1]], label='Audio (Trimmed)', color=active_color)
ax[0].set_ylabel('Amplitude')
#ax[0].set_xlabel('Sample')
#plt.legend()

ax[1].pcolor(M[:audio_transformer.n_mfcc], cmap=mfcccm, vmin=mfccvmin, vmax=mfccvmax)
#plt.legend()
ax[1].set_title('MFCC features')
ax[1].set_ylabel("mel-frequency")
ax[1].set_xlabel("one every 10 ms")

zoom_effect01(ax[0], ax[1], 0, len(M.T) , trim_idx[0], trim_idx[1])

fig.align_ylabels(ax)
plt.show()

## BIG PLOT NR 2
fig, ax = plt.subplots(3, 1, gridspec_kw={'height_ratios': [1, 1, 3]}, figsize=a4_dims)
ax[0].set_title('Raw audio sample')
ax[0].plot(range(len(audio)), audio, label='Audio', color=inactive_color)
ax[0].plot(range(trim_idx[0], trim_idx[1]), audio[trim_idx[0]:trim_idx[1]], label='Audio (Trimmed)', color=active_color)
ax[0].set_ylabel('Amplitude')
#ax[0].set_xlabel('Sample')
#plt.legend()

ax[1].pcolor(M[:audio_transformer.n_mfcc], cmap=mfcccm, vmin=mfccvmin, vmax=mfccvmax)
#plt.legend()
ax[1].set_title('MFCC features') # (win=25 ms, hop=10 ms)
ax[1].set_ylabel("Mel-frequency")
ax[1].set_xlabel("(one every 10 ms)")
ax[1].text(10, -45, '...', clip_on=False, in_layout=False, fontsize='x-large', color=inactive_color)

c1, c2, _, _, _ = zoom_effect01(ax[0], ax[1], 0, len(M.T) , trim_idx[0], trim_idx[1])
c1.set_color(active_color + (0.7,))
c2.set_color(active_color + (0.7,))

plt.pcolor(mfcc.T, cmap=mfcccm, vmin=mfccvmin, vmax=mfccvmax)

ax[2].set_title('Processed audio sample') # (win=8, hop=3)
ax[2].set_ylabel("Features")
ax[2].set_xlabel("T")

c1, c2, _, _, _ = zoom_effect01(ax[1], ax[2], 0, 1, 0, 7, 0, 1)
c1.set_color(active_color + (0.7,))
c2.set_color(active_color + (0.7,))

c1, c2, _, _, _ = zoom_effect01(ax[1], ax[2], 1, 2, 0+3, 7+3)
c1.set_color(inactive_color + (0.7,))
c2.set_color(inactive_color + (0.7,))


ax[2].xaxis.set_minor_locator(FixedLocator(range(3)))
#ax[2].yaxis.set_minor_locator(MultipleLocator(80))
ax[1].set_yticks([0, 80])
ax[2].set_yticks([0, 80*8])

fig.align_ylabels(ax)
plt.show()


# PLOT ALPHA BETA
import torch.nn.functional as F
from alpha_beta import compute_alpha, compute_beta

def alpha_beta(logit, target_text):
    logP = F.log_softmax(logit, -1)
    alphas = compute_alpha(target_text, logP.cpu().numpy())
    betas = compute_beta(target_text, logP.cpu().numpy())
    return alphas, betas

with torch.no_grad():
    logits, _ = model.forward(mfcc[None], F.pad(text_enc[None], [1, 0, 0, 0]),
                             # Padding is specified in reverse order for some reason.
                             torch.as_tensor((len(mfcc),), dtype=torch.int32), torch.as_tensor((len(text_enc),), dtype=torch.int32) + 1,
                             torch.tensor((0,), dtype=torch.int64))  # Could also use self.text_transformer.encode

alphas, betas = alpha_beta(logits[0], text_enc)

fig, ax = plt.subplots(2, 1, sharex=False, constrained_layout=True, gridspec_kw={'height_ratios': [3, 1]})
#extent = [-0.5, alphas.shape[0] - 0.5, -0.5, alphas.shape[1] - 0.5]
ax[0].pcolor(np.rot90(np.exp(alphas + betas)), cmap=mfcccm, snap=True)
ax[0].set_title('Alignment')
ax[0].set_yticks([0, len(text_translit)+1])
ax[0].set_yticklabels([str(len(text_translit)+1), str(0)])
ax[0].set_xlabel('T')
ax[0].set_ylabel('U')
text_ax = ax[0].twinx()
text_ax.set_yticks(np.array(range(alphas.shape[-1]+1)) - 0.5)
text_ax.set_yticks(np.array(range(alphas.shape[-1])), minor=True)
# ax.tick_params(axis='x', which='minor', bottom=False)
text_ax.set_yticklabels(' ' + text_translit, rotation=-90, rotation_mode='anchor',
                        ha='center', va='baseline', minor=True, in_layout=False)
text_ax.yaxis.set_tick_params(which='minor', length=0)
text_ax.grid(True, axis='y', linestyle='-', color='k')
text_ax.set_yticklabels([''] * alphas.shape[-1], minor=False)
text_ax.invert_yaxis()
#text_ax.set_ylim(*ax[0].get_ylim())  # Also inverts this axis

librosa.display.waveplot(audio_trimmed, x_axis='s', sr=22050, ax=ax[1], color=active_color)

plt.show()

fig, ax = plt.subplots(2, 1, sharex=False,
                       constrained_layout=True, gridspec_kw={'height_ratios': [3, 1]},
                       figsize=(11.7, 8.27))
#extent = [-0.5, alphas.shape[0] - 0.5, -0.5, alphas.shape[1] - 0.5]
ax[0].pcolor(np.rot90(np.exp(alphas + betas)), cmap=mfcccm, snap=True)
ax[0].set_title('Alignment')
ax[0].set_yticks([0, len(text_translit)+1])
ax[0].set_yticklabels([str(len(text_translit)+1), str(0)])
#ax[0].set_yticks(np.arange(23, 0, -1) - 0.5, minor=True)
ax[0].set_yticks(np.array(range(alphas.shape[-1])) + 0.5, minor=True)
# np.array(list(reversed(range(alphas.shape[-1]))))+1
ax[0].set_yticklabels(reversed(' ' + text_translit.upper()), rotation='vertical', rotation_mode='anchor',
                        ha='center', va='baseline', minor=True, in_layout=False, family='serif', weight='bold')
ax[0].tick_params(axis='y', which='major', pad=13)
ax[0].yaxis.set_tick_params(which='minor', length=0, labelsize=12)
#ax[0].yaxis.set_tick_params(which='minor', length=0)
ax[0].set_xlabel('T')
ax[0].set_ylabel('U')
ax[0].invert_yaxis()
ax[0].set_xticklabels([str(int(num+1)) for num in ax[0].get_xticks()])
text_ax = ax[0].twinx()
text_ax.set_yticks(np.array(range(alphas.shape[-1]+1)) - 0.5)
text_ax.yaxis.set_tick_params(which='both', length=0)
text_ax.grid(True, axis='y', linestyle='-', color='k')
text_ax.set_yticklabels([''] * alphas.shape[-1], minor=False)
text_ax.invert_yaxis()
#text_ax.set_ylim(*ax[0].get_ylim())  # Also inverts this axis
librosa.display.waveplot(audio_trimmed, x_axis='s', sr=22050, ax=ax[1], color=active_color)
#ax[1].set_yticklabels([])
plt.savefig('Alignment_new.png', transparent=False, dpi=300)
#plt.show()



import logging
logging.getLogger().setLevel(logging.INFO)
model.summarize(mode='full')


fig, ax = plt.subplots(3, 1)
ax[0].set_title('Forward (Alpha)')
ax[0].pcolor(np.rot90(alphas), cmap=mfcccm, snap=True)
ax[1].set_title('Backward (Beta)')
ax[1].pcolor(np.rot90(betas), cmap=mfcccm, snap=True)
ax[2].set_title('Alpha+Beta')
ax[2].pcolor(np.rot90((alphas+betas)), cmap=mfcccm, snap=True)
plt.show()

#val_iter = iter(model.val_loader)
#example = next(val_iter)
#next(model.val_loader)

if False:
    import seaborn as sns

    sns.set(context="poster", style='ticks')

    dims = (15, 8)

    fig, ax = plt.subplots(3, 1, gridspec_kw={'height_ratios': [1, 2, 1]}, constrained_layout=True, figsize=dims)
    textkwargs = {'fontsize': 'small'}  # {'fontname':"Nimbus Roman No9 L", 'fontweight':"bold"}
    ax[0].set_title('Raw audio sample')
    ax[0].plot(range(len(audio)), audio, label='Audio', color=inactive_color)
    ax[0].plot(range(trim_idx[0], trim_idx[1]), audio[trim_idx[0]:trim_idx[1]], label='Audio (Trimmed)',
               color=active_color)
    ax[0].set_ylabel('Amplitude', **textkwargs)
    sns.despine()
    # ax[0].set_xlabel('Sample')
    # plt.legend()

    ax[1].pcolor(M[:audio_transformer.n_mfcc], cmap=mfcccm, vmin=mfccvmin, vmax=mfccvmax)
    # plt.legend()
    ax[1].set_title('MFCC features')  # (win=25 ms, hop=10 ms)
    ax[1].set_ylabel("Mel-frequency", **textkwargs)
    ax[1].set_xlabel("(one every 10 ms)", in_layout=False, **textkwargs)
    ax[1].text(10, -45 * 2 - 10, '...', clip_on=False, in_layout=False, color=inactive_color, **textkwargs)

    c1, c2, _, _, _ = zoom_effect01(ax[0], ax[1], 0, len(M.T), trim_idx[0], trim_idx[1])
    c1.set_color(active_color + (0.7,))
    c2.set_color(active_color + (0.7,))

    plt.pcolor(mfcc.T, cmap=mfcccm, vmin=mfccvmin, vmax=mfccvmax)

    ax[2].set_title('Processed audio sample')  # (win=8, hop=3)
    ax[2].set_ylabel("Features", **textkwargs)
    ax[2].set_xlabel("T", **textkwargs)

    c1, c2, _, _, _ = zoom_effect01(ax[1], ax[2], 0, 1, 0, 7, 0, 1)
    c1.set_color(active_color + (0.7,))
    c2.set_color(active_color + (0.7,))

    c1, c2, _, _, _ = zoom_effect01(ax[1], ax[2], 1, 2, 0 + 3, 7 + 3)
    c1.set_color(inactive_color + (0.7,))
    c2.set_color(inactive_color + (0.7,))

    ax[2].xaxis.set_minor_locator(FixedLocator(range(3)))
    # ax[2].yaxis.set_minor_locator(MultipleLocator(80))
    ax[1].set_yticks([0, 80])
    ax[2].set_yticks([0, 80 * 8])

    fig.align_ylabels(ax)
    # plt.tight_layout()
    plt.savefig('Preprocessing.png', transparent=True, dpi=300)
